<?php

class Subir extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('file');
    }

    function subir_ya() {
        $config = array();
        $config['upload_path'] = './public/txt_indice/';
        $config['allowed_types'] = 'txt';
        $config['max_size'] = '5000';
        $config['file_name'] = $this->session->userdata('isbn_indice');
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload()) {
            /* $error = array('error' => $this->upload->display_errors());
              $this->load->view('subida/upload_form', $error); */
            show_error($this->upload->display_errors());
        } else {
            $recupera = read_file("./public/txt_indice/" . $config['file_name'] . ".txt");
            $this->db->where('ISBN-ISSN-codigo', $this->session->userdata('isbn_indice'));
            $this->db->update('material_bibliografico', array('indice' => $recupera));
            $recupera = unlink('./public/txt_indice/' . $config['file_name'] . '.txt');
            $query = $this->db->get_where('material_bibliografico', array('ISBN-ISSN-codigo' => $config['file_name']));
            foreach ($query->result() as $value) {
                echo nl2br($value->indice);
            }
        }
        $this->session->unset_userdata('isbn_indice');
    }

    function verifica() {
        $isbn = $this->input->post('isbn');
        $this->session->set_userdata('isbn_indice', $isbn);
        $query = $this->db->get_where('material_bibliografico', array('ISBN-ISSN-codigo' => $isbn));
        $respuesta = FALSE;
        foreach ($query->result() as $value) {
            if (strlen($value->indice) > 0) {
                $respuesta = TRUE;
            }
        }
        echo json_encode($respuesta);
    }

}

/* Fin del archivo subir.php */
