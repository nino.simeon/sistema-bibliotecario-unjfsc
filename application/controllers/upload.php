<?php

class Upload extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->helper('file');
    }

    function index() {
        $this->load->view('subida/upload_form', array('error' => ' '));
    }

    function do_upload() {
        $config['upload_path'] = './public/txt_indice/';
        $config['allowed_types'] = 'gif|jpg|png|txt';
        $config['max_size'] = '5000';
        $config['file_name'] = 'nombre_personalizado';
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            $this->load->view('subida/upload_form', $error);
        } else {
            $recupera = read_file('./public/txt_indice/nombre_personalizado.txt');
            $this->db->insert('indice', array('indice' => $recupera, 'llave' => '1'));
            $recupera = unlink('./public/txt_indice/nombre_personalizado.txt');
            $query = $this->db->get('indice');
            foreach ($query->result() as $value) {
                echo nl2br($value->indice);
            }
            die;
            $data = array('upload_data' => $this->upload->data());
            $this->load->view('subida/upload_success', $data);
        }
    }

}

/* Fin del archivo upload.php */

    