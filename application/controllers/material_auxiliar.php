<?php

class Material_auxiliar extends CI_Controller {

    var $sesion;

    function __construct() {
        parent::__construct();
        $this->load->model('Mbibliografico');
        $this->sesion = $this->session->userdata('logeado');
        $this->acceso->chrome();
    }

    function sinconfirmar() {
        $carga = array();
        if ($this->input->post('confirma')) {
            $this->Mbibliografico->confirma_item($this->input->post('confirma'));
        } else {
            $query = $this->Mbibliografico->muestra_itema($this->sesion['cod_terminal']);
            if ($query->num_rows() > 0) {
                $carga['lista'] = $query;
            } else {
                $carga['lista'] = FALSE;
            }
            $carga['menu'] = $this->acceso->menu();
            $this->load->view('auxiliar/materialbibliografico/sin_confirmar', $carga);
        }
    }
    
}

?>
