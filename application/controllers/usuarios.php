<?php

class Usuarios extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Musuarios');
        $this->load->model('Mevento');
        $this->acceso->controlar();
        $this->acceso->chrome();
    }

    function index() {
        $imprime['menu'] = $this->acceso->menu();
        $this->load->view('auxiliar/inicio', $imprime);
    }

    function actualizar() {
        $query = array();
        $datos = array();
        if ($this->input->post('consulta')) {//consulta es el DNI
            $consulta = $this->Musuarios->obtener_usuario($this->input->post('consulta'));
            if ($consulta->num_rows() > 0) {
                foreach ($consulta->result() as $value) {
                    $query['dni'] = $value->cuenta;
                    $query['apellidos'] = $value->apellidos;
                    $query['nombres'] = $value->nombres;
                    $query['carne'] = $value->carneuniv;
                    $query['sexo'] = $value->sexo;
                    $query['direccion'] = $value->dirFisica;
                    $query['mail'] = $value->email;
                    $query['telefono'] = $value->telefono;
                    $query['perfil'] = $value->perfil_usuario;
                    $query['terminal'] = $value->nomTerminal;
                    $query['codTerminal'] = $value->codTerminal;
                }
            } else {
                $query['estado'] = 'fail';
            }
            echo json_encode($query);
        } else if ($this->input->post('update')) { //update es  DNI
            if (strlen($this->input->post('clave')) > 0) {
                $actualiza = $this->Musuarios->update_usuario($this->input->post('update'), $this->input->post('carne'), $this->input->post('sexo'), $this->input->post('direccion'), $this->input->post('mail'), $this->input->post('telefono'), $this->input->post('terminal'), $this->input->post('clave'));
            } else if (strlen($this->input->post('clave')) < 1) {
                $actualiza = $this->Musuarios->update_usuario($this->input->post('update'), $this->input->post('carne'), $this->input->post('sexo'), $this->input->post('direccion'), $this->input->post('mail'), $this->input->post('telefono'), $this->input->post('terminal'));
            } else if ($actualiza != 'ok') {
                echo 'fail';
            }
        } else {
            $datos['menu'] = $this->acceso->menu();
            $query = $this->db->get('terminal');
            $datos['terminal'] = $query;
            $this->load->view('auxiliar/usuarios/actualiza', $datos);
        }
    }

    function registrar() {
        $datos = array();
        $sesion = $this->session->userdata('logeado');
        //var_dump($sesion);die;
        if ($this->input->post('inicia_registro')) {
            if ($this->Musuarios->consultaDoc($this->input->post('inicia_registro'))) {
                echo 'existe';
            } else {
                $registro = $this->Mevento->registro_usuario($this->input->post('tipoDoc'), $this->input->post('inicia_registro'), $this->input->post('carneuniv'), $this->input->post('apellidos'), $this->input->post('nombres'), $this->input->post('fechaNac'), $this->input->post('sexo'), $this->input->post('dirFisica'), $this->input->post('email'), $this->input->post('telefono'), $sesion['cuenta'], $this->input->post('clave'), $this->input->post('perfil_usuario'), $sesion['cod_terminal']);
                if ($registro == 'ok') {
                    echo 'ok';
                }
            }
        } else if ($this->input->post('consulta_documento')) {
            $consulta_doc = $this->Musuarios->consultaDoc($this->input->post('consulta_documento'));
            if ($consulta_doc) {
                echo 'existe';
            }
        } else {
            $datos['menu'] = $this->acceso->menu();
            $datos['tipoPerfil'] = $this->Musuarios->obtener_tipoPerfil();
            $datos['tipoDoc'] = $this->Musuarios->obtener_tipoDoc();
            $this->load->view('auxiliar/usuarios/registrar', $datos);
        }
    }

}

?>