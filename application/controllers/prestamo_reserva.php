<?php

class Prestamo_reserva extends CI_Controller {

    var $sesion;

    function __construct() {
        parent::__construct();
        $this->load->model('Mprestamo_reserva');
        $this->load->model('Mevento');
        $this->load->model('Musuarios');
        $this->sesion = $this->session->userdata('logeado');
        $this->acceso->chrome();
    }

    function solicitud() {
        /* var_dump($this->session->all_userdata());
          die; */
        if ($this->input->post('buscador_libros')) {
            $query = $this->Mprestamo_reserva->buscar_item($this->input->post('buscador_libros'), $this->sesion['cod_terminal']);
            $jbusca = array();
            if ($query->num_rows() > 0) {
                $i = 0;
                foreach ($query->result() as $value) {
                    $jbusca[$i] = array('value' => $value->signatura, 'label' => $value->busca);
                    $i = $i + 1;
                }
            } else {
                $jbusca[] = array('value' => 'YFT', 'label' => 'NO HAY DISPONIBLE / NO ENCONTRADO!!. =(');
            }
            echo json_encode($jbusca);
        } else if ($this->input->post('buscador_dni')) {
            $query = $this->Musuarios->obtener_dni($this->input->post('buscador_dni'));
            if ($query->num_rows() > 0) {
                $i = 0;
                foreach ($query->result() as $value) {
                    $jbusca[$i] = array('value' => $value->dni, 'label' => $value->nombres);
                    $i = $i + 1;
                }
            } else {
                $jbusca[] = array('value' => '00000000', 'label' => 'NO ENCONTRADO!! prueba ingresando directamente el DNI');
            }
            $this->output->set_content_type('json')->set_output(json_encode($jbusca));
        } else if ($this->input->post('autosolicitud')) {
            $this->Mevento->solicitud_reserva($this->input->post('lector'), $this->input->post('item'), 'SOLICITUD');
            $query = $this->db->get_where('view_reporte_inconcluso_solicitud', array('dni' => $this->input->post('lector')));
            foreach ($query->result() as $value) {
                $n_solicitud = $value->nevento;
            }
            $presta = $this->Mevento->prestamo($n_solicitud, $this->sesion['cuenta']);
            $jSabe['n_prestamo'] = $presta['nprestamo'];
            $jSabe['f_inicio'] = $presta['finicio'];
            $jSabe['h_inicio'] = $presta['hinicio'];
            $jSabe['f_fin'] = $presta['ffin'];
            $jSabe['h_fin'] = $presta['hfin'];
            echo json_encode($jSabe);
        } else {
            $datos['menu'] = $this->acceso->menu();
            $this->load->view('auxiliar/transacciones/solicitud_prestamo', $datos);
        }
    }

    function devolucion() {
        $jBusca = array();
        $jDevolucion = array();
        $jSancion = array();
        $sancion = array();
        $data = array();
        if ($this->input->post('click_busca')) {
            $query = $this->Mprestamo_reserva->click_listar_d($this->input->post('click_busca'));
            foreach ($query->result() as $value) {
                $jBusca['isbn'] = $value->idItem;
                $jBusca['identificacion'] = $value->apellidos . ' ' . $value->nombres;
            }
            echo json_encode($jBusca);
        } else if ($this->input->post('dato_devolucion')) {
            $query = $this->Mevento->devolucion($this->input->post('dato_devolucion'), $this->sesion['cuenta'], $this->input->post('item_devolucion'));
            foreach ($query->result() as $value) {
                $jDevolucion['devolucion'] = $value->nevento;
                $jDevolucion['fecha'] = $value->fecha;
                $jDevolucion['hora'] = $value->hora;
            }
            echo json_encode($jDevolucion);
        } else if ($this->input->post('dato_sancion')) {
            $query = $this->Mprestamo_reserva->antecedente($this->input->post('dato_sancion'));
            $jSancion['nveces'] = $query;
            echo json_encode($jSancion);
        } else if ($this->input->post('sancion_consuma')) {
            $query = $this->Mevento->devolucion($this->input->post('sancion_consuma'), $this->sesion['cuenta'], $this->input->post('item_devolucion'), $this->input->post('tipo_sancion'));
            foreach ($query->result() as $value) {
                $sancion['devolucion'] = $value->nevento;
                $sancion['fecha'] = $value->fecha;
                $sancion['hora'] = $value->hora;
                $sancion['nsancion'] = $value->nsancion;
                $sancion['fecha_san'] = $value->fecha_san;
                $sancion['hora_san'] = $value->hora_san;
            }
            echo json_encode($sancion);
        } else {
            $data['menu'] = $this->acceso->menu();
            $data['listado'] = $this->Mprestamo_reserva->listar_devolucion($this->sesion['cod_terminal']);
            $data['sanciones'] = $this->db->get('especificacion_castigo');
            $this->load->view('auxiliar/transacciones/lista_devolucion', $data);
        }
    }

    function prestamo() {
        $estado = '';
        $jConfirma1 = array();
        $jConfirma2 = array();
        $datos = array();
        if ($this->input->post('click_busca')) {
            $query = $this->Mprestamo_reserva->click_listar_p($this->input->post('click_busca'));
            foreach ($query->result() as $value) {
                $jBusca['identificacion'] = $value->apellidos . ' ' . $value->nombres;
                $jBusca['isbn'] = $value->idItem;
                $jBusca['estado'] = $value->estado;
                echo json_encode($jBusca);
            }
        } else if ($this->input->post('click_confirma')) {
            $disponibilidad = $this->Mprestamo_reserva->verificaDisponibilidad($this->input->post('click_confirma'));
            foreach ($disponibilidad->result() as $value) {
                $estado = $value->estado;
            }
            if ($estado == 'DISPONIBLE') {
                $query = $this->Mprestamo_reserva->prestamo_vista($this->input->post('click_confirma'));
                $responsable = $this->db->get_where('persona', array('numero' => $this->sesion['cuenta']));
                foreach ($responsable->result() as $value) {
                    $jConfirma1['responsable'] = $value->apellidos . ' ' . $value->nombres;
                }
                foreach ($query->result() as $value) {
                    $jConfirma1['usuario'] = $value->apellidos . ' ' . $value->nombres;
                    $jConfirma1['item'] = $value->idItem;
                    $jConfirma1['prestamo'] = $value->modPrestamo;
                }
            } else {
                $jConfirma1['estado'] = 'fail';
            }
            echo json_encode($jConfirma1);
        } else if ($this->input->post('click_elimina')) {
            $this->Mprestamo_reserva->anula($this->input->post('click_elimina'));
        } else if ($this->input->post('confirmado')) {
            $prestamo = $this->Mevento->prestamo($this->input->post('confirmado'), $this->sesion['cuenta']);

            $jConfirma2['nprestamo'] = $prestamo['nprestamo'];
            $jConfirma2['finicio'] = $prestamo['finicio'];
            $jConfirma2['hinicio'] = $prestamo['hinicio'];
            $jConfirma2['ffin'] = $prestamo['ffin'];
            $jConfirma2['hfin'] = $prestamo['hfin'];

            echo json_encode($jConfirma2);
        } else {
            $datos['menu'] = $this->acceso->menu();
            $datos['listado'] = $this->Mprestamo_reserva->listar_prestamo($this->sesion['cod_terminal']);
            $this->load->view('auxiliar/transacciones/lista_prestamo', $datos);
        }
    }

}

?>
