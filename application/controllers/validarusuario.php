<?php

class Validarusuario extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Mvalidarusuario');
        if ($this->session->userdata('logeado')) {
            $this->manejaUsuario();
        }
    }

    function index() {
        $data = array();
        $data['browser'] = '';
        if ($this->agent->is_browser('MSIE', 'Internet Explorer')) {
            /* $data['browser'] = '<img id="browser" src="<?php echo base_url(); ?>public/img/browser.jpg" width="400px" height="300px" onclick="elige_browser()"/>'; */
            $datos_browser = array(
                'src' => base_url('public/img/ie_error.jpg'),
                'alt' => 'Tu puedes navegar mejor, LO SÉ',
                'id' => 'browser',
                'title' => 'Cambia de navegador',
                'alt' => 'Cambia de navegador',                
                'onclick' => 'elige_browser()'
            );
            $data['browser'] = img($datos_browser);
        }
        $this->load->view('validar_usuario/inicio', $data);
    }

    function logear() {
        $sesion = array();
        if ($this->input->post('user')) {
            $query = $this->Mvalidarusuario->validar($this->input->post('user'), $this->input->post('pass'));
            if ($query) {
                foreach ($query->result() as $value) {
                    $sesion ['cuenta'] = $value->cuenta;
                    $sesion ['perfil_usuario'] = $value->perfil_usuario;
                    $sesion ['ciclo'] = $this->Mvalidarusuario->obtenerciclo();
                    $sesion ['cod_terminal'] = $value->codTerminal;
                    $sesion ['nom_terminal'] = $this->Mvalidarusuario->determinaTerminal($value->codTerminal);
                    $sesion ['apellidos_nombres'] = $this->Mvalidarusuario->nombres($value->cuenta);
                    $this->session->set_userdata('logeado', $sesion);
                }
            } else {
                echo 'error';
            }
        } else {
            $sesion ['cuenta'] = 'ANONIMO';
            $sesion ['perfil_usuario'] = 'INVITADO';
            $sesion ['apellidos_nombres'] = 'ANONIMO';
            $sesion ['ciclo'] = $this->Mvalidarusuario->obtenerciclo();
            $sesion ['cod_terminal'] = 'TERM-INGENIERIA';
            $sesion ['nom_terminal'] = $this->Mvalidarusuario->determinaTerminal($sesion ['cod_terminal']);
            $this->session->set_userdata('logeado', $sesion);
            redirect('validarusuario/manejausuario');
        }
    }

    function manejaUsuario() {
        if ($this->session->userdata('logeado')) {
            $perfil = $this->session->userdata('logeado');
            switch ($perfil ['perfil_usuario']) {
                case 'LECTOR' :
                    redirect('visualizarcatalogo', 'refresh');
                    break;
                case 'INVITADO' :
                    redirect('visualizarcatalogo', 'refresh');
                    break;
                case 'AUXILIAR' :
                    redirect('usuarios', 'refresh');
                    break;
                case 'BIBLIOTECARIO' :
                    redirect('bibliografico', 'refresh');
                    break;
                case 'DIRECTOR' :
                    redirect('reporte', 'refresh');
                    break;
                case 'ADMIN' :
                    redirect('reporte', 'refresh');
                    break;
                default :
                    break;
            }
        } else {
            redirect('validarusuario', 'refresh');
        }
    }

}

?>
