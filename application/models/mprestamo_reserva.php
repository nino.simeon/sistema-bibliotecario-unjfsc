<?php

class Mprestamo_reserva extends CI_Model {

    function buscar_item($frase, $terminal) {
        $query = $this->db->query("SELECT 
                                   CONCAT(material_bibliografico.autores,', ',material_bibliografico.titulo,'. Ed.',material_bibliografico.edicion) AS busca,
                                    item_bibliografico.signatura
                                   FROM
                                    item_bibliografico
                                   INNER JOIN material_bibliografico ON item_bibliografico.`ISBN-ISSN-codigo` = material_bibliografico.`ISBN-ISSN-codigo`
                                   WHERE 
                                    titulo LIKE '%$frase%' OR autores LIKE '%$frase%' AND item_bibliografico.estado = 'DISPONIBLE' AND
                                    item_bibliografico.modPrestamo = 'INTERNO' AND
                                    item_bibliografico.codTerminal = '$terminal'
                                   GROUP BY
                                    item_bibliografico.`ISBN-ISSN-codigo`");
        return $query;
    }

    function click_listar_d($numero) {
        $this->db->select('persona.apellidos, persona.nombres, evento_solicitud.idItem');
        $this->db->from('evento_prestamo');
        $this->db->where('evento_prestamo.nevento', $numero);
        $this->db->join('evento_solicitud', 'evento_prestamo.nsolicitud = evento_solicitud.nevento');
        $this->db->join('evento', 'evento_solicitud.nevento = evento.numero');
        $this->db->join('usuario', 'evento.usuario = usuario.cuenta');
        $this->db->join('persona', 'usuario.cuenta = persona.numero');
        $query = $this->db->get();
        return $query;
    }

    function antecedente($nevento) {
        $this->db->select('evento.usuario, evento_prestamo.nevento');
        $this->db->from('evento');
        $this->db->join('evento_solicitud', 'evento_solicitud.nevento = evento.numero');
        $this->db->join('evento_prestamo', 'evento_prestamo.nsolicitud = evento_solicitud.nevento');
        $this->db->where('evento_prestamo.nevento', $nevento);
        $query = $this->db->get();
        foreach ($query->result() as $value) {
            $dni = $value->usuario;
        }
        $consulta = $this->db->get_where('view_sancion_verifica', array('dni' => $dni));
        return $consulta->num_rows();
    }

    function listar_devolucion($terminal, $limite = NULL, $offset = NULL, $usuario = NULL) {
        $this->db->select('evento_prestamo.nevento, evento.usuario, evento_solicitud.idItem, item_bibliografico.modPrestamo, evento_prestamo.fechaFin, evento_prestamo.horaFin');
        $this->db->from('evento_prestamo');
        if ($usuario) {
            $this->db->where('evento.usuario', $usuario);
        }
        /* ACA FILTRAMOS POR TERMINAL :D
         */
        $this->db->where('item_bibliografico.codTerminal', $terminal);
        /* FIN DE FILTRO POR TERMINAL ^^
         */
        $this->db->where('evento_prestamo.estado', 'SIN DEVOLVER');
        $this->db->join('evento_solicitud', 'evento_prestamo.nsolicitud = evento_solicitud.nevento');
        $this->db->join('item_bibliografico', 'evento_solicitud.idItem = item_bibliografico.signatura');
        $this->db->join('evento', 'evento_solicitud.nevento = evento.numero');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return FALSE;
        }
    }

    function listar_prestamo($terminal, $limite = NULL, $offset = NULL, $usuario = NULL) {
        //agregar filtro por terminal =)
        $this->db->select('evento_solicitud.nevento, evento_solicitud.tipoSolicitud,evento_solicitud.idItem,evento.usuario, evento.fecha, evento.hora');
        $this->db->from('evento_solicitud');
        $this->db->where('evento_solicitud.estado', 'SIN CONFIRMAR');
        if ($usuario) {
            $this->db->where('evento.usuario', $usuario);
        }
        $this->db->where('item_bibliografico.codTerminal', $terminal);
        //$this->db->where('item_bibliografico.codTerminal', $terminal);
        $this->db->join('evento', 'evento_solicitud.nevento = evento.numero');
        $this->db->join('item_bibliografico', 'evento_solicitud.idItem = item_bibliografico.signatura');
        $this->db->limit($limite, $offset);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return FALSE;
        }
    }

    function verificaDisponibilidad($numero) {
        $this->db->select('item_bibliografico.estado');
        $this->db->from('evento_solicitud');
        $this->db->where('evento_solicitud.nevento', $numero);
        $this->db->join('item_bibliografico', 'evento_solicitud.idItem = item_bibliografico.signatura');
        $query = $this->db->get();
        return $query;
    }

    function prestamo_vista($numero) {
        $this->db->select('evento_solicitud.idItem, item_bibliografico.modPrestamo, persona.apellidos, persona.nombres');
        $this->db->from('evento_solicitud');
        $this->db->where('evento.numero', $numero);
        $this->db->join('evento', 'evento_solicitud.nevento = evento.numero');
        $this->db->join('usuario', 'evento.usuario = usuario.cuenta');
        $this->db->join('item_bibliografico', 'evento_solicitud.idItem = item_bibliografico.signatura');
        $this->db->join('persona', 'usuario.cuenta = persona.numero');
        $query = $this->db->get();
        return $query;
    }

    function anula($numero) {
        $dato = array();
        $this->db->trans_start();
        $dato['estado'] = 'ANULADO';
        $this->db->where('nevento', $numero);
        $this->db->update('evento_solicitud', $dato);
        echo 'ok';
        $this->db->trans_complete();
    }

    function click_listar_p($numero) {
        $this->db->select('evento_solicitud.idItem, item_bibliografico.estado, persona.apellidos, persona.nombres, evento.usuario');
        $this->db->from('evento_solicitud');
        $this->db->where('evento_solicitud.nevento', $numero);
        $this->db->join('evento', 'evento_solicitud.nevento = evento.numero');
        $this->db->join('item_bibliografico', 'evento_solicitud.idItem = item_bibliografico.signatura');
        $this->db->join('usuario', 'evento.usuario = usuario.cuenta');
        $this->db->join('persona', 'usuario.cuenta = persona.numero');
        $query = $this->db->get();
        return $query;
    }

}

?>
