<?php

class Mbibliografico extends CI_Model {

    function buscador_alternativo($parametro) {
        /* $this->db->select('CONCAT(material_bibliografico.autores,material_bibliografico.titulo) AS busca, material_bibliografico.`ISBN-ISSN-codigo` AS isbn');
          $this->db->from('material_bibliografico');
          $this->db->like('titulo', $parametro);
          $this->db->or_like('autores', $parametro);
          $query = $this->db->get(); */
        $query = $this->db->query("SELECT CONCAT(material_bibliografico.autores,', ',material_bibliografico.titulo,'. Ed.',material_bibliografico.edicion) AS busca, material_bibliografico.`ISBN-ISSN-codigo` AS isbn
                FROM material_bibliografico
                WHERE titulo LIKE '%$parametro%' OR autores LIKE '%$parametro%'");
        return $query;
    }

    function ingresa_ebook($isbn, $titulo, $categoria, $tematica, $autor, $publicacion, $edicion, $signatura, $terminal, $resolucion = NULL, $volumen = NULL) {
        $inserta = array();
        $aplica = array();
        $yacasi = array();
        $this->db->trans_start();
        $inserta['ISBN-ISSN-codigo'] = $isbn;
        $inserta['titulo'] = $titulo;
        $inserta['categoria'] = $categoria;
        $inserta['codTematica'] = $tematica;
        $inserta['autores'] = $autor;
        $inserta['editorial-empresa'] = 'PERSONAL';
        $inserta['fecPublicacion'] = $publicacion;
        $inserta['edicion'] = $edicion;
        $inserta['contenido'] = "public/pdf/" . $isbn . ".pdf";
        if (strlen($volumen) > 0) {
            $inserta['volumen'] = $volumen;
        }
        $this->db->insert('material_bibliografico', $inserta);
        $aplica['signatura'] = $signatura;
        $aplica['soporte'] = 'EBOOK';
        $aplica['estado'] = 'DISPONIBLE';
        $aplica['ISBN-ISSN-codigo'] = $isbn;
        $aplica['modPrestamo'] = 'EXTERNO';
        $aplica['codTerminal'] = $terminal;
        $aplica['origen'] = 'DONACION';
        $aplica['observaciones'] = $autor;
        if (strlen($resolucion) > 0) {
            $aplica['resolucion'] = $resolucion;
        }
        $this->db->insert('item_bibliografico', $aplica);
        $yacasi['idItem'] = $signatura;
        $query = $this->db->get('ciclo_academico');
        foreach ($query->result() as $value) {
            $maxCiclo = $value->codigo;
        }
        $yacasi['codigoCiclo'] = $maxCiclo;
        $this->db->insert('item_ciclo', $yacasi);        
        $this->db->trans_complete();
    }

    function ingresa_material($isbn, $titulo, $categoria, $tematica, $autores, $editorial, $fecha, $edicion, $volumen = NULL) {
        $parametros = array();
        $this->db->trans_start();
        $parametros['ISBN-ISSN-codigo'] = $isbn;
        $parametros['titulo'] = $titulo;
        $parametros['categoria'] = $categoria;
        $parametros['codTematica'] = $tematica;
        $parametros['autores'] = $autores;
        $parametros['editorial-empresa'] = $editorial;
        $parametros['fecPublicacion'] = $fecha;
        $parametros['edicion'] = $edicion;
        if (mb_strlen($volumen) > 0) {
            $parametros['volumen'] = $volumen;
        }
        $this->db->insert('material_bibliografico', $parametros);
        echo 'ok';
        $this->db->trans_complete();
    }

    function ingresa_item($isbn, $soporte, $prestamo, $origen, $observaciones, $signatura, $terminal, $resolucion = NULL) {
        $item = array();
        $item_ciclo = array();
        $this->db->trans_start();
        $item['signatura'] = $signatura;
        $item['codTerminal'] = $terminal;
        $item['ISBN-ISSN-codigo'] = $isbn;
        $item['estado'] = 'SIN CONFIRMAR'; //CASO INGRESO MASIVO. SINO ES (SIN CONFIRMAR - DISPONIBLE)
        $item['soporte'] = $soporte;
        $item['modPrestamo'] = $prestamo;
        $item['origen'] = $origen;
        $item['observaciones'] = $observaciones;
        $item['resolucion'] = $resolucion;
        $this->db->insert('item_bibliografico', $item);
        $this->db->select_max('codigo');
        $query = $this->db->get('ciclo_academico');
        foreach ($query->result() as $value) {
            $maxCiclo = $value->codigo;
        }
        $item_ciclo['codigoCiclo'] = $maxCiclo;
        $item_ciclo['idItem'] = $item['signatura'];
        $this->db->insert('item_ciclo', $item_ciclo);
        $this->db->trans_complete();
    }

    function describe_material($isbn) {
        $query = $this->db->get_where('material_bibliografico', array('ISBN-ISSN-codigo' => $isbn));
        return $query;
    }

    function verifica_existencia($isbn) {
        $query = $this->db->get_where('material_bibliografico', array('ISBN-ISSN-codigo' => $isbn));
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function verifica_categoria($categoria) {
        $query = $this->db->get_where('categoria', array('categoria' => $categoria));
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            $this->db->insert('categoria', array('categoria' => $categoria));
            return TRUE;
        }
    }

    function muestra_itema($terminal) {
        $query = $this->db->get_where('view_item_sinconfirmar', array('codTerminal' => $terminal, 'estado' => 'SIN CONFIRMAR'));
        return $query;
    }

    function confirma_item($signatura) {
        $this->db->where('signatura', $signatura);
        $this->db->update('item_bibliografico', array('estado' => 'DISPONIBLE'));
    }

}

?>
