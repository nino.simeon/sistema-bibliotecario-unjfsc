<?php

class Mevento extends CI_Model {

    function verificaSolicitudAnterior($user) {
        $this->db->select('evento_solicitud.estado, evento.usuario');
        $this->db->from('evento_solicitud');
        $this->db->where('usuario', $user);
        $this->db->where('estado', 'SIN CONFIRMAR');
        $this->db->join('evento', 'evento_solicitud.nevento=evento.numero');
        $consulta_solicitud = $this->db->get();
        // Aca pendiente agregar codigo le para verificacion de prestamo actual
        // :D
        $this->db->select('*');
        $this->db->from('evento_prestamo');
        $this->db->where('evento.usuario', $user);
        $this->db->where('evento_prestamo.estado', 'SIN DEVOLVER');
        $this->db->join('evento_solicitud', 'evento_prestamo.nsolicitud = evento_solicitud.nevento');
        $this->db->join('evento', 'evento_solicitud.nevento = evento.numero');
        $consulta_prestamo = $this->db->get();
        // Aca detectamos si es TRUE o FALSE
        $detector = $consulta_solicitud->num_rows() + $consulta_prestamo->num_rows();
        if ($detector > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function ultimo_evento() {
        $ultimo = 0;
        $this->db->select_max('numero');
        $query = $this->db->get('evento');
        foreach ($query->result() as $value) {
            $ultimo = $value->numero;
        }
        return $ultimo;
    }

    function devolucion($numero, $responsable, $item, $tipo_sancion = NULL) {
        $evento = array();
        $devolucion = array();
        $evento_s = array();
        $sancion = array();
        $this->db->trans_start();
        $lastID = $this->ultimo_evento();
        $evento ['numero'] = $lastID + 1;
        $evento ['usuario'] = $responsable;
        $this->db->insert('evento', $evento);
        $devolucion ['nevento'] = $evento ['numero'];
        $devolucion ['idPrestamo'] = $numero;
        $this->db->insert('evento_devolucion', $devolucion);
        $this->db->where('nevento', $numero);
        $this->db->update('evento_prestamo', array('estado' => 'DEVUELTO'));
        $this->db->where('signatura', $item);
        $this->db->update('item_bibliografico', array('estado' => 'DISPONIBLE'));
        if ($tipo_sancion != NULL) {
            // var_dump($evento['numero']+1);die;
            $evento_s ['numero'] = $evento ['numero'] + 1;
            $evento_s ['usuario'] = $evento ['usuario']; //Determinar el sancionado
            $this->db->insert('evento', $evento_s);
            $sancion ['nevento'] = $evento_s ['numero'];
            $sancion ['tipoCastigo'] = $tipo_sancion;
            /*
             * DETERMINAMOS EL TIEMPO SEGUN SERVER DE BD - DE ENTREGA
             */
            $time = $this->db->get_where('evento', array('numero' => $evento_s ['numero']));
            foreach ($time->result() as $value) {
                $horae = $value->hora;
                $fechae = $value->fecha;
            }
            /*
             * DETERMINAMOS EL TIEMPO SEGUN SERVER DE BD - DE ENTREGA
             */
            $tiempo_castigo = $this->db->get_where('especificacion_castigo', array('codigo' => $tipo_sancion));
            foreach ($tiempo_castigo->result() as $value) {
                $fecha = $value->diasCastigo;
            }
            $sancion ['fecFinCastigo'] = date("Y-m-d", strtotime("$fechae + $fecha days"));
            $sancion ['horFinCastigo'] = $horae;
            $this->db->insert('evento_sancion', $sancion);
            $this->db->where('evento_devolucion.nevento', $evento ['numero']);
            $this->db->update('evento_devolucion', array('nsancion' => $evento_s ['numero']));
            /*             * ******************************* */
            $sancionado = $this->db->get_where('view_sancion_final', array('evento_sancion' => $evento_s ['numero']));
            foreach ($sancionado->result() as $value) {
                $dni_sancionado = $value->dni;
            }
            $this->db->where('cuenta', $dni_sancionado);
            $this->db->update('usuario', array('estado' => 'DESHABILITADO'));
            // pendiente
        }
        $this->db->trans_complete();
        if ($tipo_sancion != NULL) {
            $query = $this->db->get_where('view_sniffer_sancion', array('nsancion' => $sancion ['nevento']));
        } else {
            $this->db->select('evento_devolucion.nevento, evento.fecha, evento.hora');
            $this->db->from('evento_devolucion');
            $this->db->where('evento_devolucion.nevento', $evento ['numero']);
            $this->db->join('evento', 'evento_devolucion.nevento = evento.numero');
            $query = $this->db->get();
        }
        return $query;
    }

    function prestamo($numero, $responsable) {
        $evento = array();
        $tipo = '';
        $prestamo = array();
        $isbn = '';
        $imprime = array();
        $this->db->trans_start();
        $lastID = $this->ultimo_evento();
        $evento ['numero'] = $lastID + 1;
        $evento ['usuario'] = $responsable;
        $this->db->insert('evento', $evento);
        /*
         * DETERMINAMOS EL TIEMPO SEGUN SERVER DE BD
         */
        $time = $this->db->get_where('evento', array('numero' => $evento ['numero']));
        foreach ($time->result() as $value) {
            $horai = $value->hora;
            $fechai = $value->fecha;
        }
        /*
         * DETERMINAMOS EL TIEMPO SEGUN SERVER DE BD
         */
        $prestamo ['nevento'] = $evento ['numero'];
        $prestamo ['nsolicitud'] = $numero;
        /*
         * ACA DETERMINAMOS EL TIPO DE PRESTAMO (CONSULTAR HORARIO DE ATENCION)
         */
        $tipoPrestamo = $this->determinaTipoPrestamo($numero);
        foreach ($tipoPrestamo->result() as $value) {
            $tipo = $value->modPrestamo;
            $isbn = $value->idItem;
        }
        if ($tipo == 'INTERNO') {
            $prestamo ['fechaFin'] = $fechai;
            $prestamo ['horaFin'] = date("H:i:s", mktime(22, 00, 00));
        } else {
            $prestamo ['fechaFin'] = date("Y-m-d", strtotime("$fechai + 2 days"));
            $prestamo ['horaFin'] = $horai;
        }
        /*
         * ACA DETERMINAMOS EL TIPO DE PRESTAMO (CONSULTAR HORARIO DE ATENCION)
         */
        $prestamo ['estado'] = 'SIN DEVOLVER';
        $this->db->insert('evento_prestamo', $prestamo);
        /*
         * PONEMOS AL ITEM COMO 'NO DISPONIBLE' :D
         */
        $this->db->where('signatura', $isbn);
        $this->db->update('item_bibliografico', array('estado' => 'NO DISPONIBLE'));
        /*
         * PONEMOS AL ITEM COMO 'NO DISPONIBLE' :D
         */

        // ACA PONEMOS A LA SOLICITUD COMO CONFIRMADO
        $this->db->where('nevento', $numero);
        $this->db->update('evento_solicitud', array('estado' => 'CONFIRMADO'));
        $this->db->trans_complete();
        $imprime ['nprestamo'] = $evento ['numero'];
        $imprime ['finicio'] = $fechai;
        $imprime ['hinicio'] = $horai;
        $imprime ['ffin'] = $prestamo ['fechaFin'];
        $imprime ['hfin'] = $prestamo ['horaFin'];
        return $imprime;
    }

    function determinaTipoPrestamo($numero) {
        $this->db->select('item_bibliografico.modPrestamo, evento_solicitud.idItem');
        $this->db->from('evento_solicitud');
        $this->db->where('evento_solicitud.nevento', $numero);
        $this->db->join('item_bibliografico', 'evento_solicitud.idItem = item_bibliografico.signatura');
        $query = $this->db->get();
        return $query;
    }

    function solicitud_reserva($lector, $item, $tipo) {
        $this->db->trans_start();
        $lastID = $this->ultimo_evento();
        $evento ['numero'] = $lastID + 1;
        $evento ['usuario'] = $lector;
        $this->db->insert('evento', $evento);
        $solicitud ['nevento'] = $evento ['numero'];
        if ($tipo == 'SOLICITUD') {
            $solicitud ['tipoSolicitud'] = 'SOLICITUD';
        } else {
            $solicitud ['tipoSolicitud'] = 'RESERVA';
        }
        $solicitud ['estado'] = 'SIN CONFIRMAR';
        // $solicitud['lector'] = $lector;
        $solicitud ['idItem'] = $item;
        $this->db->insert('evento_solicitud', $solicitud);
        $this->db->trans_complete();
    }

    function registro_usuario($tipoDoc, $numero, $carneuniv, $apellidos, $nombres, $fechaNac, $sexo, $dirFisica, $email, $telefono, $cuenta, $clave, $perfil_usuario, $cod_terminal) {
        $this->db->trans_start();
        $lastID = $this->ultimo_evento();
        $evento ['numero'] = $lastID + 1;
        $evento ['usuario'] = $cuenta;
        $this->db->insert('evento', $evento);
        /*
         * AÑADIENDO EN CASO PIDA BOLETA <INICIO>
         */
        $evento_registro ['nevento'] = $evento ['numero'];
        $this->db->insert('evento_registro', $evento_registro);
        /*
         * AÑADIENDO EN CASO PIDA BOLETA <FIN>
         */
        $registro ['nregistro'] = $evento ['numero'];
        $registro ['tipoDoc'] = $tipoDoc;
        $registro ['numero'] = $numero;
        $registro ['carneuniv'] = $carneuniv;
        $registro ['apellidos'] = $apellidos;
        $registro ['nombres'] = $nombres;
        $registro ['fechaNac'] = $fechaNac;
        $registro ['sexo'] = $sexo;
        $registro ['dirFisica'] = $dirFisica;
        $registro ['email'] = $email;
        $registro ['telefono'] = $telefono;
        $this->db->insert('persona', $registro);
        $usuario ['cuenta'] = $numero;
        $usuario ['clave'] = $this->encrypt->encode($clave);
        $usuario ['estado'] = 'HABILITADO';
        $usuario ['perfil_usuario'] = $perfil_usuario;
        $usuario ['codTerminal'] = $cod_terminal;
        $this->db->insert('usuario', $usuario);
        echo 'ok';
        $this->db->trans_complete();
        // return 'ok';
    }

}

?>
