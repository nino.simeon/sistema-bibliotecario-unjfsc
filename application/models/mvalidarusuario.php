<?php

class Mvalidarusuario extends CI_Model {

    function sancionado($dni) {
        $this->db->where('dni', $dni);
        $this->db->order_by('fechaFin', 'desc');
        $this->db->limit(1);
        $query = $this->db->get('view_sancion_final');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $value) {
                $fecha_final = $value->fechaFin;
            }
            if (time() > strtotime($fecha_final)) {
                $this->db->where('cuenta', $dni);
                $this->db->update('usuario', array('estado' => 'HABILITADO'));
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return TRUE;
        }
    }

    function validar($user, $pass) {
        #Esta puesto en comentario las otras sentencias debido a que no cuentan con encriptación
        /* $query = $this->db->get_where('usuario', array('cuenta' => $user, 'clave' => $pass, 'estado' => 'HABILITADO')); */
        #Arriesgamos quitando HABILITADO para que los sancionados puedan visualizar la lista mas NO solicitarlo
        /* $query = $this->db->get_where('usuario', array('cuenta' => $user, 'estado' => 'HABILITADO')); */
        $query = $this->db->get_where('usuario', array('cuenta' => $user));
        foreach ($query->result() as $value) {
            if ($pass == $this->encrypt->decode($value->clave)) {
                return $query;
            } else {
                return FALSE;
            }
        }
        /* if ($query->num_rows() == 1) {
          return $query;
          } else {
          return FALSE;
          } */
    }

    function obtenerciclo() {
        $ciclo = '';
        $this->db->select_max('codigo');
        $query = $this->db->get('ciclo_academico');
        foreach ($query->result() as $value) {
            $ciclo = $value->codigo;
        }
        return $ciclo;
    }

    function nombres($user) {
        $this->db->select('persona.apellidos, persona.nombres');
        $this->db->from('usuario');
        $this->db->join('persona', 'usuario.cuenta=persona.numero');
        $this->db->where('usuario.cuenta', $user);
        $query = $this->db->get();
        foreach ($query->result() as $value) {
            $apellido = $value->apellidos;
            $nombres = $value->nombres;
        }
        $persona = $apellido . ' ' . $nombres;
        return $persona;
    }

    function determinaTerminal($terminal) {
        $query = $this->db->get_where('terminal', array('codTerminal' => $terminal));
        foreach ($query->result() as $value) {
            $terminal = $value->nomTerminal;
        }
        return $terminal;
    }

}

?>
