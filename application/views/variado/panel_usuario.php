<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta name="description" content="Sistema Web Bibliotecario UNJFSC" />
        <title>..::Sistema Bibliotecario::..</title>
        <link href="<?php echo base_url('public/css/estilosBusqueda.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('public/css/fresh_theme.css'); ?>" rel="stylesheet" type="text/css" />
        <link rel="icon" href="<?php echo base_url('public/img/favicon.ico'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/ui.jqgrid.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.ui.sunny.css'); ?>"/>        
        <script src="<?php echo base_url('public/lib/jquery.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.datepicker-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.sunny.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/grid.locale-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.jqGrid.min.js'); ?>" type="text/javascript" ></script>
        <script>
            $().ready(function(){
                $('#b_direccion').click(function(){
                    $.post('<?php echo site_url('variado/panel'); ?>',{direccion: $('#t_direccion').text()});
                });
                $('#b_mail').click(function(){
                    $.post('<?php echo site_url('variado/panel'); ?>',{mail: $('#t_mail').text()});
                });
                $('#b_telefono').click(function(){
                    $.post('<?php echo site_url('variado/panel'); ?>',{telefono: $('#t_telefono').text()});
                });
                $('#creditos').click(function(){
                    $('#msg_ninosimeon').dialog('open');
                });           
                $('#change_password').click(function(){
                    $('#password span').show('slow');                     
                });
                $('#password span').hide();
                $('#msg_ninosimeon').dialog({
                    autoOpen: false,
                    show: 'blind',
                    hide: 'explode',
                    title: 'Créditos',
                    width: 300
                });              
            });    
            function cambiar_pass(){
                if(($('#p_new').val()==$('#p_r_new').val())&&($('#p_r_new').val().length > 1)){                    
                    $.post('<?php echo site_url('variado/panel'); ?>',{password:true,inicial:$('#p_origen').val(),nuevo:$('#p_new').val()},function(r){                        
                        if(r.estado == 'fail'){
                            alert('Contraseña erronea','Try again');
                        }else if(r.estado == 'ok'){
                            $('#password span').hide();                     
                            alert('Contraseña cambiada satisfactoriamente', 'Success');
                        } else{
                            alert('Error en el Sistema, Sorry =(');  
                        }                 
                    },'json');                   
                }else if($('#p_r_new').val().length > 1){
                    alert('Son contraseñas distintas ¬¬!','¿Are you kidding me?');                              
                }
                return false;                
            }
        </script>
    </head>
    <div id="msg_ninosimeon">
        <p>
            Desarrollador: <b>Nino David Simeón Huaccho</b><br /> <br /> <b>mail:</b>
            ninosimeon@gmail.com<br /> <b>url:</b> <a
                href="http://about.me/ninosimeon">about.me/ninosimeon</a><br />
        </p>
        <h4>E.A.P. Ing. Informática "Alan Turing"</h4>
    </div>
    <body>
        <div id="contenedor" class="ui-widget">
            <header>
                <a href="<?php echo site_url(); ?>"><img src="<?php echo base_url('public/img/banner_optimizado/bannerReporte_r1_c1.jpg'); ?>" width="221" height="67" alt="bannerReporte_r1_c1" /><img src="<?php echo base_url('public/img/banner_optimizado/bannerReporte_r1_c2.jpg'); ?>" width="297" height="67" alt="bannerReporte_r1_c2" /><img src="<?php echo base_url('public/img/banner_optimizado/bannerReporte_r1_c3.jpg'); ?>" width="432" height="67" alt="bannerReporte_r1_c3" /></a>
            </header>
            <section>
                <div id="imagen">
                    <img src="<?php echo base_url('public/img/sin_foto.jpg'); ?>" width="220" height="165" alt="TU FOTO" />
                </div>
                <div id="descripcion_usuario" class="derecha ui-widget-header" style="width: 685px;">
                    <b><?php echo $persona['cargo']; ?>,</b> <?php echo $persona['nombres']; ?> <nav style="margin-right: 10px;">
                        <a href="<?php echo site_url('variado/panel'); ?>">
                            Panel de usuario</a>
                        | <a href="<?php echo site_url('variado/cerrar_sesion'); ?>">Cerrar
                            Sesión</a>
                    </nav>
                </div>
                <!--<div id="descripcion_usuario" class="derecha">
                    <b><?php echo $persona['cargo']; ?>,</b> <?php echo $persona['nombres']; ?> <nav>
                        <a href="<?php echo site_url('variado/panel'); ?>">Panel de usuario</a>
                        | <a href="<?php echo site_url('variado/cerrar_sesion'); ?>">Cerrar Sesión</a>
                    </nav>
                </div>-->                
                <div id="detalles_incambiables" class="derecha">  
                    <p>
                        NOMBRES: <b><?php echo htmlentities($panel_persona['nombres'], ENT_QUOTES, 'UTF-8'); ?></b><br />
                        FACULTAD: <b><?php echo htmlentities($panel_persona['ubicacion'], ENT_QUOTES, 'UTF-8'); ?></b><br />
                        FECHA DE NACIMIENTO: <b><?php echo htmlentities($panel_persona['fecha'], ENT_QUOTES, 'UTF-8'); ?></b>                        
                    </p>
                </div>
                <article id="detalles_panel" class="panel">
                    <hr />
                    <p style="font-size: 1.2em;">
                        <b>DIRECCIÓN:</b> <span contenteditable="true" id="t_direccion"><?php
if (strlen($panel_persona['direccion'])) {
    echo htmlentities($panel_persona['direccion'], ENT_QUOTES, 'UTF-8');
} else {
    echo 'Cambia tu dirección directamente aqui ;)';
}
?></span><button type="button" id="b_direccion">๑</button><br />
                        <b>EMAIL:</b> <span contenteditable="true" id="t_mail"><?php
                            if (strlen($panel_persona['mail'])) {
                                echo htmlentities($panel_persona['mail'], ENT_QUOTES, 'UTF-8');
                            } else {
                                echo 'cambiaya@hotmail.com';
                            }
?></span><button type="button" id="b_mail">๑</button><br />
                        <b>TELEFONO:</b> <span contenteditable="true" id="t_telefono"><?php
                            if (strlen($panel_persona['telefono'])) {
                                echo htmlentities($panel_persona['telefono']);
                            } else {
                                echo '5555555555555';
                            }
?></span><button type="button" id="b_telefono">๑</button><br />
                        <!--<?php echo form_open(); ?>
                    <b>FOTO: <?php echo form_upload(array('name' => 'subida', 'required' => 'true', 'style' => 'width:350px;')); ?><button type="submit">๑</button></b>
                    <br /><b id="problema_subida"></b>
                        <?php echo form_close(); ?>-->
                    </p>
                    <hr />                    
                    <form id="password" onsubmit="return cambiar_pass()">
                        <p>
                            <span>
                                <b>CONTRASEÑA ACTUAL:</b> <input id="p_origen" type="password" required/><br />
                                <b>CONTRASEÑA NUEVA:</b> <input id="p_new" type="password" required/><br />
                                <b>REPITA CONTRASEÑA NUEVA:</b> <input id="p_r_new" type="password" required/>
                            </span>
                            <button id="change_password" type="submit">CAMBIAR CONTRASEÑA</button><br />                        
                        </p>
                    </form>

                    <hr />
                </article>
                <footer class="ui-state-default">
                    <div style="float: left;">
                        Ciudad Universitaria - Av. Mercedes Indacochea N° 609<br />
                        Teléfono: 232-1338, Huacho - Perú
                    </div>
                    <div style="float: right">Desarrollado por: Nino D. Simeón Huaccho</div>                    
                    <div style="clear: both;"></div>
                </footer>
            </section>
        </div>
    </body>
</html>
