<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta name="description" content="Sistema Web Bibliotecario UNJFSC" />
        <title>..::Sistema Bibliotecario::..</title>
        <link href="<?php echo base_url('public/css/reporte.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('public/css/fresh_theme.css'); ?>" rel="stylesheet" type="text/css" />
        <link rel="icon" href="<?php echo base_url('public/img/favicon.ico'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/ui.jqgrid.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.ui.sunny.css'); ?>"/>        
        <script src="<?php echo base_url('public/lib/jquery.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.datepicker-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.sunny.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/highcharts.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('public/lib/grid.locale-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.jqGrid.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/exporting.js'); ?>" type="text/javascript"></script>                      
        <script>               
            var chart_operacion;
            $().ready(function(){    
                $('button').button()
                $('.seleccion').button({
                    icons:{
                        primary: "ui-icon-arrowreturnthick-1-e"
                    }
                })      
                $('.seleccion_another').button({
                    icons:{
                        primary: "ui-icon ui-icon-circle-triangle-e"
                    }
                })            
                $('#tabla_jqgrid').jqGrid({
                    url:'<?php echo site_url('reporte/jqgrid_sancion_v2'); ?>',
                    datatype: 'json',
                    mtype: 'post',
                    colNames:['DNI','NOMBRES', 'FECHA INICIO','HORA INICIO','FECHA FIN', 'HORA FIN','TERMINAL','DESCRIPCION'],
                    colModel:[
                        {name:'DNI',index:'dni', width: 70},
                        {name:'NOMBRES',index:'nombres', width: 280},
                        {name:'FECHA INICIO',index:'fechaInicio', sortable: true, width: 80},
                        {name:'FECHA FIN',index:'horaInicio', sortable: true, width: 70, align: 'center'},
                        {name: 'FECHA FIN', index: 'fechaFin', sortable: true, width: 70},
                        {name: 'HORA FIN', index: 'horaFin', sortable: true, width: 70},                            	
                        {name: 'TERMINAL', index: 'terminal', sortable: true, width: 120},        
                        {name: 'DESCRIPCION', index: 'descripcion', sortable: true, width: 100}                            
                    ],  
                    height: 400,
                    pager: '#pie_jqgrid',
                    rowNum: 20,
                    rowList: [10,20,30],
                    sortname: 'fechaInicio',
                    viewrecords: true,
                    sortorder: "asc",
                    gridview: true,
                    caption:"Relación de Sancionados",
                    /*onSelectRow: function(id){                            
                            $('#libro_jqgrid').jqGrid('setGridParam',{url:"<?php echo site_url('reporte/jqgrid_operaciones_libro'); ?>/"+id, page: 1}).trigger("reloadGrid").setGridState('visible');                 
                        }*/                                                                                           
                }); 
                $("#tabla_jqgrid").jqGrid('navGrid','#pie_jqgrid',{search: true, edit: false, del: false, add: false}).trigger("reloadGrid");
                
                $('button[id=btn_ir]').button({
                    icons:{
                        primary: "ui-icon-circle-arrow-e"
                    },
                    text: true
                }).click(function(){      
                    //Grafico de solicitantes :)
                    $('button[id=btn_descarga_pdf]').show();
                    chart_operacion = new Highcharts.Chart({
                        chart: {
                            renderTo: 'contenedor_operaciones',                        
                            type: 'column',
                            events: {
                                load: carga_operaciones()
                            }
                        },
                        title: {
                            text: ''
                        },legend: {
                            enabled: false  
                        },
                        tooltip: {
                            formatter: function() {
                                return '<b>Año '+ this.x +'</b>: '+ this.y+' items';
                            }
                        },xAxis:{
                            categories: [],
                            title: {
                                text: null
                            }
                        },yAxis:{
                            title:{
                                text: null
                            }
                        },
                        series: [{                                      
                                data: []
                            }]
                    });                         
                                                                                           
                });             
                $('button[id=btn_descarga_pdf]').button({
                    icons:{
                        primary: "ui-icon-disk"
                    }
                }).click(function(){
                    document.location.href= '<?= site_url('reporte/pdf_publicacion') ?>';
                });
                $('#menu_reporte ul').menu();
                $('#buscador_libros').hide();                
                $('#msg_nino').dialog({
                    autoOpen: false,
                    show: 'blind',
                    hide: 'explode',
                    title: 'Información',
                    width: 300,
                    modal: true
                },'json');                
                $('#nino').click(function(){					
                    $('#msg_nino').dialog('open');
                });                      
            });         

            function carga_operaciones(){                
                $.post('<?php echo site_url('reporte/operaciones'); ?>',{anio: $('#select_anio').val()},function(r){
                    chart_operacion.xAxis[0].setCategories(r.terminal);                    
                    for (i = 0;r.cantidad.length; i++) {                          
                        chart_operacion.series[0].addPoint(r.cantidad[i]);                         
                    }    
                },'json');                                                                            
            }       
        </script>
    </head>
    <body>        
        <div id="msg_nino"><p>Nino Simeón, promoción &quot;Alan Turing&quot;, E.A.P. Ing. Informática 2010 - II</p><p>e-mail: ninosimeon@gmail.com<br />
                web: <a href="http://about.me/ninosimeon">about.me/ninosimeon</a></p></div>
        <div id="contenedors" class="ui-widget">            
            <div id="bannerTOP"><img src="<?php echo base_url(); ?>public/img/banner_reporte/bannerReporte_mostrar_r1_c1.jpg" width="216" height="67" /><img src="<?php echo base_url(); ?>public/img/banner_reporte/bannerReporte_mostrar_r1_c2.jpg" width="193" height="67" /><img src="<?php echo base_url(); ?>public/img/banner_reporte/bannerReporte_mostrar_r1_c3.jpg" width="290" height="67" /><img src="<?php echo base_url(); ?>public/img/banner_reporte/bannerReporte_mostrar_r1_c4.jpg" width="201" height="67" /></div>
            <div id="superior">
                <div id="menu_reporte" class="">
                    <div>
                        <h4 class="ui-widget-header ui-corner-top">MENÚ</h4>
                        <div class="ui-widget-content ui-corner-bottom">                 
                            <?php echo anchor('reporte', "<button class='seleccion'>Inicio</button>"); ?><br>
                            <?php echo anchor('reporte/item_ingreso', "<button class='seleccion'>Items ingreso</button>"); ?><br>
                            <?php echo anchor('reporte/operaciones', "<button class='seleccion'>Operaciones</button>"); ?><br>
                            <?php echo anchor('reporte/item_publicacion', "<button class='seleccion'>Items publicación</button>"); ?><br>
                            <?php echo anchor('reporte/sancion', "<button class='seleccion'>Sanción</button>"); ?><br>
                            <?php echo anchor('reporte/resumen', "<button class='seleccion'>Resumen</button>"); ?>
                        </div>                                                         
                    </div>
                    <div id="otros_menu" class="" style="margin-top: 10px;">
                        <?php echo $menu; ?>                                 
                    </div>                    
                </div>            
                <div id="descripcion_usuario" class="ui-widget-header" style="width: 685px;">
                    <?= $sesion; ?> <nav style="margin-right: 10px;">
                        <a href="<?php echo site_url('variado/panel'); ?>">
                            Panel de usuario</a>
                        | <a href="<?php echo site_url('variado/cerrar_sesion'); ?>">Cerrar
                            Sesión</a>
                    </nav>
                </div>  
                <div id="tipo_reporte">REPORTE SANCIÓN</div>
                <div id="buscador_libros">ISBN:
                    <input type="text" name="input_isbn" id="input_isbn" style="width:140px"/>
                </div>
                <div id="descripcion_reporte">
                    <div id="saludo_reporte">
                        <p><strong>Sr. bibliotecario</strong>, a continuación se muestra por terminal los items fecha de publicación.</p>
                        <p>Si desea realizar un seguimiento mucho más exahustivo, puede realizar una búsqueda personalizada que permitira filtrar los resultados.
                        </p>
                        <p>Por Ej: <b>DNI - igual a - 46280004</b></p>
                        <!--<button id="btn_descarga_pdf">Descargar PDF</button>-->
                    </div>
                    <!--<div id="detalle_reporte">Si desea ver el reporte completo <b>descarge el PDF</b>, caso contrario dirigase en el menú de navegación y seleccione el terminal a investigar :)
                    </div>-->
                </div>
            </div>
            <div id="contenido_elemento">                            
                <div style="clear: both;"></div>
                <div id="contenido_jqgrid">
                    <table id="tabla_jqgrid"></table>
                    <div id="pie_jqgrid"></div>
                </div>
            </div>
            <footer id="footer" class="ui-state-default">
                <div style="float: left;">
                    Ciudad Universitaria - Av. Mercedes Indacochea N° 609<br />
                    Teléfono: 232-1338, Huacho - Perú
                </div>
                <div style="float: right">Desarrollado por: Nino D. Simeón Huaccho</div>                    
                <div style="clear: both;"></div>
            </footer>
        </div>
    </body>
</html>