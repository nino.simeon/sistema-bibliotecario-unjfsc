<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>..::Sistema Bibliotecario::..</title>
        <script type="text/javascript" src="<?php echo base_url(); ?>public/lib/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>public/lib/jquery.alerts.js" type="text/javascript"></script>        
        <link href="<?php echo base_url(); ?>public/css/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />      
        <script src="<?php echo base_url(); ?>public/lib/jquery.alphanumeric.js" type="text/javascript"></script>
        <script type="text/javascript">          
        </script>
        <link href="../../../../public/css/temaBibliotecaAuxiliar.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>public/css/temaBibliotecaAuxiliar.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">
            $().ready(function(){
                $('#buscador').hide();
                $("#nino").click(function(){
                    jAlert('Promoción "Alan Turing" E.A.P. Ing. Informática<br /><br /><a href="http://about.me/ninosimeon">+ Info</a>','Nino Simeón');
                }); 
            });
            function elige(data){
                jAlert(data);
            }
        </script>
    </head>
    <body>    
        <div id="contenido">
            <div id="buscador">Usuario: 
                <input type="text" name="usuarioBusca" id="usuarioBusca" />
                <input type="button" name="ir" id="ir" value="Ir" onclick="buscar($('#usuarioBusca').val())" /><div id="resultado" style="color: #000">
                    <p><b>No encontrado! =(</b></p>
                </div></div>
            <div id="titulo"><strong>ESTADÍSTICO</strong></div>
            <div id="cabezera"><img src="<?php echo base_url(); ?>public/img/bannerAdministrativo.png" width="800" height="67" alt="banner" />
                <div id="reportemenu"><table width="100%" border="0" cellspacing="3" cellpadding="0">
                        <tr>
                            <td colspan="2" align="center" bgcolor="#3D3D3D" scope="col"><strong>REPORTES</strong></td>
                        </tr>
                        <tr>
                            <td width="11%" align="right" valign="middle"><img src="<?php echo base_url(); ?>public/img/btn.jpg" width="14" height="17" /></td>
                            <td width="89%"><a href="<?php echo site_url('reporte'); ?>">Inicio</a></td>
                        </tr>
                        <tr>
                            <td align="right"><img src="<?php echo base_url(); ?>public/img/btn.jpg" alt="" width="14" height="17" /></td>
                            <td><a href="<?php echo site_url('reporte/operaciones'); ?>">Operaciones</a></td>
                        </tr>
                        <tr>
                            <td align="right"><img src="<?php echo base_url(); ?>public/img/btn.jpg" alt="" width="14" height="17" /></td>
                            <td><a href="<?php echo site_url('reporte/estadistico'); ?>">Estadístico</a></td>
                        </tr>
                        <tr>
                            <td align="right"><img src="<?php echo base_url(); ?>public/img/btn.jpg" alt="" width="14" height="17" /></td>
                            <td><a href="<?php echo site_url('reporte/resumen'); ?>">Resumen</a></td>
                        </tr>
                    </table></div>
            </div>
            <div id="pieDePagina">Desarrollado por: <strong><a href="#" id="nino">Nino D. Simeón Huaccho</a></strong><a href="#"></a><br />
                Ciudad Universitaria - Av. Mercedes Indacochea N 609<br />
                Teléfono: 232-1338, Huacho - Perú<br />
            </div>
            <div id="logeado">
                <table width="100%" border="0" cellspacing="1" cellpadding="0">
                    <tr>
                        <td width="63%" scope="col"><?php
$sesion = $this->session->userdata('logeado');
echo '<b>' . $sesion['perfil_usuario'] . '</b>, ' . $sesion['apellidos_nombres'];
?></td>
                        <td width="22%" scope="col"><a href="#">Cambiar contraseña</a></td>
                        <td width="15%" scope="col"><a href="<?php echo site_url('variado/cerrar_sesion'); ?>">Cerrar Sesión</a></td>
                    </tr>
                </table>
            </div>
            <div id="contenido_contenido">
                <p>Por favor seleccione el terminal a investigar:</p>
                <table width="100%" cellspacing="0" cellpadding="2">
                    <tr>
                        <th width="50%" bgcolor="#F0F0F0" scope="col">NOMBRE TERMINAL</th>
                        <th width="50%" bgcolor="#F0F0F0" scope="col">N° ITEMS REGISTRADOS</th>
                    </tr>
                    <?php foreach ($relacion->result() as $value) {
                        ?>
                        <tr>
                            <td width="50%" align="center"><a href="#" onclick="elige('<?php echo $value->codigo; ?>')"><?php echo $value->terminal; ?></a></td>
                            <td width="50%" align="center"><?php echo $value->cantidad; ?></td>
                        </tr><?php } ?>
                </table>
            </div>
        </div>
    </body>
</html>