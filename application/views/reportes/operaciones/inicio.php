<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta name="description" content="Sistema Web Bibliotecario UNJFSC" />
        <title>..::Sistema Bibliotecario::..</title>
        <link href="<?php echo base_url('public/css/reporte.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('public/css/fresh_theme.css'); ?>" rel="stylesheet" type="text/css" />
        <link rel="icon" href="<?php echo base_url('public/img/favicon.ico'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/ui.jqgrid.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.ui.sunny.css'); ?>"/>        
        <script src="<?php echo base_url('public/lib/jquery.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.datepicker-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.sunny.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/highcharts.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('public/lib/grid.locale-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.jqGrid.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/exporting.js'); ?>" type="text/javascript"></script>                                         
        <script>   
            var chart_operacion;
            $().ready(function(){       
                $('#input_libro').autocomplete({
                    source: function(request, response){                        
                        $.post('<?php echo site_url('bibliografico/add_sin_isbn'); ?>',{buscador_frases: request.term},function(r){                            
                            response(r);                         
                        },'json');
                    },
                    minLength: 3
                })
                $('#input_persona').autocomplete({                    
                    source: function(request, response){                        
                        $.post('<?php echo site_url('prestamo_reserva/solicitud'); ?>',{buscador_dni: request.term},function(r){                            
                            response(r);                         
                        },'json');
                    },
                    minLength: 3                    
                })
                $('#form_busca').submit(function(){                                                                
                    if ($('#fecha_2').val().length > 0) {
                        if ($('#fecha_1').val().length == 0) {
                            alert('No se puede proceder, FALTA LA FECHA 1')                            
                        }
                        else if($('#fecha_2').val() < $('#fecha_1').val()){
                            alert('¡PROBLEMA! ¿Fecha 1 > Fecha 2?')
                        }              
                        else {
                            inicia_reporte()
                        }          
                    }
                    else{
                        inicia_reporte()
                    }                    
                    return false
                })
                $('.fecha').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'yy-mm-dd',
                    option: $.datepicker.regional['es'],
                    yearRange: '1910:2020',
                    showAnim: 'slide'
                })             
                $('button').button()
                $('.seleccion').button({
                    icons:{
                        primary: "ui-icon-arrowreturnthick-1-e"
                    }
                })      
                $('.seleccion_another').button({
                    icons:{
                        primary: "ui-icon ui-icon-circle-triangle-e"
                    }
                })                    
                $('button[id=btn_ir]').button({
                    icons:{
                        primary: "ui-icon-circle-arrow-e"
                    },
                    text: true
                })
                $('button[id=btn_descarga_pdf]').button({
                    icons:{
                        primary: "ui-icon-disk"
                    }
                }).click(function(){
                    window.open('<?php echo site_url('reporte/pdf_operaciones'); ?>');                    
                });
                $('#menu_reporte ul').menu();
                $('#buscador_libros').hide();                
                $('#msg_nino').dialog({
                    autoOpen: false,
                    show: 'blind',
                    hide: 'explode',
                    title: 'Información',
                    width: 300,
                    modal: true
                },'json');                
                $('#nino').click(function(){					
                    $('#msg_nino').dialog('open');
                });                      
            });         

            function carga_operaciones(){                
                $.post('<?php echo site_url('reporte/operaciones'); ?>',{anio: $('#select_anio').val()},function(r){
                    chart_operacion.xAxis[0].setCategories(r.terminal);                    
                    for (i = 0;r.cantidad.length; i++) {                          
                        chart_operacion.series[0].addPoint(r.cantidad[i]);                         
                    }    
                },'json');                                                                            
            }

            function inicia_reporte(){
                $.post('<?php echo site_url('reporte/operaciones'); ?>', {reporte: true, fecha_1: $('#fecha_1').val(), fecha_2: $('#fecha_2').val(), libro: $('#input_libro').val(), persona: $('#input_persona').val(), terminal: $('#input_terminal').val()},function(){
                    //INICIA EVENTO GRAFICA JQGRID
                    //Grafico de solicitantes :)                        
                    //Añadir tiempo de espera para que refresque y carge los datos                    
                    $('#libro_jqgrid').jqGrid({
                        url: '<?php echo site_url('reporte/jqgrid_operaciones_libro'); ?>',
                        height: 30,
                        datatype: 'JSON',
                        mtype: 'POST',
                        colNames: ['AUTOR','TITULO','PRESTAMO','TERMINAL','SIGNATURA', 'ISBN'],
                        colModel: [
                            {name: 'AUTOR', index: 'autor', width:170},
                            {name: 'TITULO', index: 'titulo', width:310},                            
                            {name: 'PRESTAMO', index: 'prestamo', width:80}, 
                            {name: 'TERMINAL', index: 'terminal', width: 100},
                            {name: 'SIGNATURA', index: 'signatura', width: 110},
                            {name: 'ISBN', index: 'isbn', width: 90}
                        ],
                        pager: '#pie_libro',
                        caption: 'Libro'
                    }).navGrid('#pie_libro',{search: false, edit: false, del: false, add: false}).setGridState('hidden');
                    $('#tabla_jqgrid').jqGrid({
                        url:'<?php echo site_url('reporte/jqgrid_operaciones'); ?>',
                        datatype: 'JSON',
                        mtype: 'POST',
                        colNames:['DNI','NOMBRES', 'FECHA','HORA','FACULTAD', 'EVENTO', 'ITEM'],
                        colModel:[
                            {name:'DNI',index:'DNI', width: 80},
                            {name:'NOMBRES',index:'NOMBRES', width: 320},
                            {name:'fecha',index:'fecha', sortable: true, width: 50},
                            {name:'hora',index:'hora', sortable: true, width: 70, align: 'center'},
                            {name: 'FACULTAD', index: 'FACULTAD', sortable: true, width: 130},
                            {name: 'numero', index: 'numero', sortable: true, width: 40},
                            {name: 'signatura', index: 'signatura', sortable: true, width: 145}
                        ],  
                        height: 400,
                        pager: '#pie_jqgrid',
                        rowNum: 20,
                        rowList: [10,20,30],
                        sortname: 'fecha',
                        viewrecords: true,
                        sortorder: "asc",
                        gridview: true,
                        caption:"Relación de Operaciones concluidas",
                        onSelectRow: function(id){                            
                            $('#libro_jqgrid').jqGrid('setGridParam',{url:"<?php echo site_url('reporte/jqgrid_operaciones_libro'); ?>/"+id, page: 1}).trigger("reloadGrid").setGridState('visible');                 
                        },
                        subGrid: true,
                        subGridUrl: '<?php echo site_url('reporte/subgrid_operaciones'); ?>',
                        subGridModel: [{name: ['DNI','NOMBRES','FECHA','HORA','TIPO','N° EVENTO'],
                                width: [100,300,50,70,100,70]}]
                    }); 
                    $("#tabla_jqgrid").jqGrid('navGrid','#pie_jqgrid',{search: true, edit: false, del: false, add: false}).trigger("reloadGrid");                                                                       
                })
            }

        </script>
        <style>
            .ui-autocomplete{
                max-height: 120px;
                overflow-y: auto;
                overflow-x: hidden;
                padding-right: 20px
            }        
        </style>
    </head>
    <body>
        <div id="contenedors" class="ui-widget">            
            <div id="bannerTOP"><img src="<?php echo base_url(); ?>public/img/banner_reporte/bannerReporte_mostrar_r1_c1.jpg" width="216" height="67" /><img src="<?php echo base_url(); ?>public/img/banner_reporte/bannerReporte_mostrar_r1_c2.jpg" width="193" height="67" /><img src="<?php echo base_url(); ?>public/img/banner_reporte/bannerReporte_mostrar_r1_c3.jpg" width="290" height="67" /><img src="<?php echo base_url(); ?>public/img/banner_reporte/bannerReporte_mostrar_r1_c4.jpg" width="201" height="67" /></div>
            <div id="superior">
                <div id="menu_reporte" class="">
                    <div>
                        <h4 class="ui-widget-header ui-corner-top">MENÚ</h4>
                        <div class="ui-widget-content ui-corner-bottom">                 
                            <?php echo anchor('reporte', "<button class='seleccion'>Inicio</button>"); ?><br>
                            <?php echo anchor('reporte/item_ingreso', "<button class='seleccion'>Items ingreso</button>"); ?><br>
                            <?php echo anchor('reporte/operaciones', "<button class='seleccion'>Operaciones</button>"); ?><br>
                            <?php echo anchor('reporte/item_publicacion', "<button class='seleccion'>Items publicación</button>"); ?><br>
                            <?php echo anchor('reporte/sancion', "<button class='seleccion'>Sanción</button>"); ?><br>
                            <?php echo anchor('reporte/resumen', "<button class='seleccion'>Resumen</button>"); ?>
                        </div>                                                         
                    </div>
                    <div id="otros_menu" class="" style="margin-top: 10px;">
                        <?php echo $menu; ?>                                 
                    </div>                    
                </div>            
                <div id="descripcion_usuario" class="ui-widget-header" style="width: 685px;">
                    <?= $sesion; ?> <nav style="margin-right: 10px;">
                        <a href="<?php echo site_url('variado/panel'); ?>">
                            Panel de usuario</a>
                        | <a href="<?php echo site_url('variado/cerrar_sesion'); ?>">Cerrar
                            Sesión</a>
                    </nav>
                </div>
                <div id="tipo_reporte">REPORTE OPERACIONES</div>
                <div id="buscador_libros">ISBN:
                    <input type="text" name="input_isbn" id="input_isbn" style="width:140px"/>
                </div>
                <div id="descripcion_reporte">
                    <div id="saludo_reporte"><strong>Sr. bibliotecario</strong>, a continuación se muestra por terminal los items fecha de publicación. 
                        <button id="btn_descarga_pdf">Descargar PDF</button>
                    </div>
                    <div id="detalle_reporte">Si desea ver el reporte completo <b>descarge el PDF</b>, caso contrario dirigase en el menú de navegación y seleccione el terminal a investigar :)
                    </div>
                    <div id="form_selector" class="ui-widget-content ui-corner-all">
                        <form id="form_busca">
                            <label>FECHA: </label><input id="fecha_1" type="date" placeholder="2012-03-12" size="8" class="fecha"> á <input id="fecha_2" type="date" placeholder="2012-03-13" size="8" class="fecha">
                            <label>TERMINAL: </label>
                            <select id="input_terminal">
                                <option value="INGENIERÍA">INGENIERÍA</option>                                                            
                            </select>
                            <button type="submit" id="btn_ir">Reporte</button>
                            <br>
                            <label>LIBRO: </label><input id="input_libro" type="text" placeholder="TÍTULO de la obra" size="25">
                            <label>PERSONA: </label><input id="input_persona" type="text" placeholder="APELLIDOS/NOMBRES" size="25">                                                            
                        </form>                        
                    </div><!-- / -->
                    <!--<div id="form_selector" class="ui-widget-content ui-corner-all">TERMINAL: 
                        <select name="select_anio" id="select_anio">                        
                            <?php foreach ($relacion_anio->result() as $value) { ?>
                                <option value="<?php echo $value->anio; ?>"><?php echo $value->anio; ?></option>
                            <?php } ?>
                        </select>
                        <button id="btn_ir">Iniciar reporte</button>
                        <br />
                    </div>-->
                    <div id="contenedor_operaciones" style="width:600px;float: right;height: 140px;margin-right: 15px;margin-top: 10px;">
                    </div>
                </div>
            </div>
            <div id="contenido_elemento">
                <div id="contenido_jqgrid">
                    <div style="clear: both;"></div>
                    <table id="libro_jqgrid"></table>
                    <div id="pie_libro"></div><br>
                    <table id="tabla_jqgrid"></table>
                    <div id="pie_jqgrid"></div>
                </div>
            </div>
            <footer id="footer" class="ui-state-default">
                <div style="float: left;">
                    Ciudad Universitaria - Av. Mercedes Indacochea N° 609<br />
                    Teléfono: 232-1338, Huacho - Perú
                </div>
                <div style="float: right">Desarrollado por: Nino D. Simeón Huaccho</div>                    
                <div style="clear: both;"></div>
            </footer>
        </div>
    </body>
</html>