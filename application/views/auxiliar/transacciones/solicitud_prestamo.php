<?php $sesion = $this->session->userdata ( 'logeado' ); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta name="description" content="Sistema Web Bibliotecario UNJFSC" />  
        <title>..::Sistema Bibliotecario::..</title>
        <link href="<?php echo base_url('public/css/temaBibliotecaAuxiliar.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('public/css/fresh_theme.css'); ?>" rel="stylesheet" type="text/css" />
        <link rel="icon" href="<?php echo base_url('public/img/favicon.ico'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/ui.jqgrid.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.ui.sunny.css'); ?>"/>        
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.alerts.css'); ?>"/>        
        <script src="<?php echo base_url('public/lib/jquery.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.datepicker-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.sunny.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/grid.locale-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.jqGrid.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.alerts.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.alphanumeric.js'); ?>" type="text/javascript" ></script>
        <script>     
            $().ready(function(){  
                $('button').button()
                $('.seleccion').button({
                    icons:{
                            primary: "ui-icon-arrowreturnthick-1-e"
                    }
                })      
                $('.seleccion_another').button({
                    icons:{
                        primary: "ui-icon ui-icon-circle-triangle-e"
                    }
                })
                $('#inicia_solicitud').click(function(){
                    $.post('<?php echo site_url('prestamo_reserva/solicitud'); ?>',{autosolicitud: true, lector: $('#input_dni').val(), item: $('#input_item').val()},function(r){
                        $('#inicia_solicitud').attr('disabled',true);
                        $('#input_item').attr('readonly',true);   
                        $('#lectura_evento').text(r.n_prestamo);
                        $('#lectura_inicio').text(r.f_inicio+' '+r.h_inicio);
                        $('#lectura_fin').text(r.f_fin+' '+r.h_fin);
                        $('#fin_soli').show('slow');
                    },'json');
                });
                $('#info_signatura').click(function(){
                    $.post('<?php echo site_url('variado/ver_item'); ?>',{item: $('#input_item').val()},function(r){
                        if (r.isbn) {
                            jAlert('<b>ISBN: </b>'+r.isbn+'<br /><b>TITULO: </b>'+r.titulo+'<br /><b>AUTOR: </b>'+r.autor+'<br /><b>EDITORIAL: </b>'+r.editorial+'<br /><b>TEMATICA: </b>'+r.tematica+'<br /><b>FECHA: </b>'+r.fecha+'<br /><b>TERMINAL: </b>'+r.terminal+'<br />', 'INFORMACION BIBLIOGRÁFICA');
                        }
                    },'json');
                });
                $('#input_item').autocomplete({                    
                    source: function(request, response){                        
                        $.post('<?php echo site_url('prestamo_reserva/solicitud'); ?>',{buscador_libros: request.term},function(r){                            
                            response(r);                         
                        },'json');
                    },
                    minLength: 3                    
                });
                $('#fin_soli').hide();
                $('#nino').click(function(){
                    $('#msg_ninosimeon').dialog('open');
                });
                $('#msg_ninosimeon').dialog({
                    autoOpen: false,
                    show: 'blind',
                    hide: 'explode',
                    title: 'Créditos',
                    width: 300
                });
                $('#info_signatura').hide();
                $('#inicia_solicitud').attr('disabled',true);
                /*$('#input_dni').numeric();*/
                $('#input_dni').autocomplete({                    
                    source: function(request, response){                        
                        $.post('<?php echo site_url('prestamo_reserva/solicitud'); ?>',{buscador_dni: request.term},function(r){                            
                            response(r);                         
                        },'json');
                    },
                    minLength: 3,
                    select: function(event, ui){
                        if (ui.item.value == 'YFT') {
                            $('.material').hide();
                            $('#agregar_material').show();
                        } else {                         
                            $('#msg_confirmar h1').empty();
                            $('#msg_confirmar h1').append(ui.item.value);
                            $('#msg_confirmar').dialog('open');                                                        
                        }
                    }                    
                });
                $('#recupera_oculto').click(function(){
                    alert($('#input_dni').val());
                });
                $('#input_item').attr('disabled',true);
                $('#search_dni').click(function(){                    
                    if($('#input_dni').val().length==8){                        
                        $.post('<?php echo site_url('bibliografico/ebook'); ?>',{verifica_dni: $('#input_dni').val()},function(r){
                            if(r == 'fail'){
                                alert('¡DNI NO ENCONTRADO!');
                            }else if(r == 'pendiente'){
                                alert('¡PRESTAMO/SOLICITUD PENDIENTE!');
                            }else{
                                $('#input_item').attr('disabled',false);
                                $('#inicia_solicitud').attr('disabled',false);
                                $('#nombre_dni').text(r);    
                                $('#input_dni').hide();
                                $('#search_dni').hide();
                                $('#lbl_dni').text('LECTOR:');
                                $('#info_signatura').show();                                
                            }
                        },'json');                        
                    }else{
                        alert('Caracteres incorrectos!');
                    }                 
                });
            });
        </script>
        <style>
            .lectura_final{
                font-size: 2em;
                font-weight: bolder;
                color: #8A8170;
            }
            .ui-autocomplete{
                max-height: 120px;
                overflow-y: auto;
                overflow-x: hidden;
                padding-right: 20px
            }
            #descripcion {
                border: 1px dashed #000;
                height: 12%;
                padding-top: 3px;
                padding-right: 1px;
                padding-bottom: 1px;
                padding-left: 1px;
                background-color: #6CF;
            }
        </style>
    </head>
    <div id="msg_ninosimeon">
        <p>
            Desarrollador: <b>Nino David Simeón Huaccho</b><br /> <br /> <b>mail:</b>
            ninosimeon@gmail.com<br /> <b>url:</b> <a
                href="http://about.me/ninosimeon">about.me/ninosimeon</a><br />
        </p>
        <h4>E.A.P. Ing. Informática "Alan Turing"</h4>
    </div>
    <body>    
        <div id="contenido" class="ui-widget">            
            <div id="titulo"><strong>AUTO SOLICITUD</strong></div>
            <div id="cabezera"><img src="<?php echo base_url(); ?>public/img/bannerAdministrativo.png" width="800" height="67" alt="banner" /></div>
            <div id="menu" class="">
                <div>
                    <h4 class="ui-widget-header ui-corner-top">USUARIOS</h4>
                    <div class="ui-widget-content">                 
                        <?php echo anchor('usuarios/registrar',"<button class='seleccion'>Registrar</button>"); ?><br>
                        <?php echo anchor('usuarios/actualizar',"<button class='seleccion'>Actualizar</button>"); ?>
                    </div>
                    <h4 class="ui-widget-header">TRANSACCIONES</h4>
                    <div class="ui-widget-content">
                        <?php echo anchor('prestamo_reserva/solicitud',"<button class='seleccion'>Solicitud prestamo</button>"); ?><br>
                        <?php echo anchor('prestamo_reserva/prestamo',"<button class='seleccion'>Lista prestamo</button>"); ?><br>
                        <?php echo anchor('prestamo_reserva/devolucion',"<button class='seleccion'>Lista devolución</button>"); ?>
                    </div>
                    <h4 class="ui-widget-header">TRANSACCIONES</h4>
                    <div class="ui-widget-content ui-corner-bottom">
                        <?php echo anchor('material_auxiliar/sinconfirmar',"<button class='seleccion'>Sin confirmar</button>"); ?>                        
                    </div>              
                </div>
                <div id="otros_menu" class="" style="margin-top: 10px;">
                    <?php echo $menu; ?>                                 
                </div>
                <div id="terminal" class="ui-corner-all ui-widget-content">
                    TERMINAL:<br> 
                    <b><?php echo $sesion['nom_terminal']; ?></b>
                </div>
            </div>
            <footer id="pieDePagina" class="ui-state-default">
                <div style="float: left;">
                    Ciudad Universitaria - Av. Mercedes Indacochea N° 609<br />
                    Teléfono: 232-1338, Huacho - Perú
                </div>
                <div style="float: right">Desarrollado por: Nino D. Simeón Huaccho</div>                    
                <div style="clear: both;"></div>
            </footer>
            <div id="logeado" class="ui-widget-header">         
                <b><?php echo $sesion ['perfil_usuario'] ; ?>,</b> <?php echo $sesion ['apellidos_nombres']; ?> 
                <nav style="margin-right: 10px;float: right;">
                    <a href="<?php echo site_url('variado/panel'); ?>">Panel de usuario</a> | 
                    <a href="<?php echo site_url('variado/cerrar_sesion'); ?>">Cerrar Sesión</a>
                </nav>
            </div>
            <div id="terminal">TERMINAL:<br />
                <strong><?php echo $sesion['nom_terminal']; ?></strong></div>
            <div id="contenido_contenido">                
                <form id="inicia_soli">
                    <input type="hidden" name="dni" id="guarda_dni" />
                    <table class="ui-widget-content ui-corner-all">
                        <tr>
                            <td><b><label for="input_dni" id="lbl_dni">DNI:</label></b></td>
                            <td><span id="nombre_dni" style="font-size: 1.5em;font-weight: bold "></span><input name="input_dni" id="input_dni" type="text" required autocomplete="off" style="width: 200px;" placeholder="46464646"/><button type="button" id="search_dni">Buscar</button></td>
                        </tr>
                        <tr>
                            <td><b><label for="input_item">ITEM:</label></b></td>
                            <td><input name="input_item" id="input_item" type="text" required autocomplete="off" style="width: 200px;"/><button type="button" id="info_signatura">¿INFO?</button></td>
                        </tr>
                        <tr>
                            <td colspan="2"><button type="button" style="width: 100%;" id="inicia_solicitud">Iniciar Solicitud</button></td>
                        </tr>
                    </table>
                </form>
                <div id="fin_soli">                    
                    <p>
                        <b>NÚMERO DE PRÉSTAMO:</b> <span class="lectura_final" id="lectura_evento">652</span><br />
                        <b>TIEMPO INICIO:</b> <span class="lectura_final" id="lectura_inicio">2012-01-15 | 22:56</span><br />
                        <b>TIEMPO FIN:</b> <span class="lectura_final" id="lectura_fin">2012-01-15 | 22:56</span>                        
                    </p>
                </div>
            </div>
        </div>
    </body>
</html>