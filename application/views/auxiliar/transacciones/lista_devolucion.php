<?php $sesion = $this->session->userdata('logeado'); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8" />
        <meta name="description" content="Sistema Web Bibliotecario UNJFSC" />  
        <title>..::Sistema Bibliotecario::..</title>
        <link href="<?php echo base_url('public/css/temaBibliotecaAuxiliar.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('public/css/fresh_theme.css'); ?>" rel="stylesheet" type="text/css" />
        <link rel="icon" href="<?php echo base_url('public/img/favicon.ico'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/ui.jqgrid.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.ui.sunny.css'); ?>"/>        
        <link rel="stylesheet" href="<?php echo base_url('public/css/jquery.alerts.css'); ?>"/>        
        <script src="<?php echo base_url('public/lib/jquery.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.datepicker-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.ui.sunny.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/grid.locale-es.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.jqGrid.min.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.alerts.js'); ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('public/lib/jquery.alphanumeric.js'); ?>" type="text/javascript" ></script>
        <script>   
            function vista_devolucion(data){
                $('.lista_devolucion').hide();
                $('#devolucion').show();
                $('#sancion').hide();
                $('#btn_aceptar_devolucion').hide();
                $('#btn_rdevolucion').attr('disabled',true);
                $('#output_prestamo').val(data);
                $('#buscador').hide();
                $('#btn_rdevolucion').attr('onclick','devolucion('+data+')');
            }
            function sancion(data){                
                $.post('<?php echo site_url('prestamo_reserva/devolucion'); ?>',{sancion_consuma: data, item_devolucion: $('#output_item').val(), tipo_sancion: $('#input_tipo').val()},function(r){
                    if(r){
                        $('#output_devolucion').val(r.devolucion);
                        $('#output_fentrega').val(r.fecha);
                        $('#output_hentrega').val(r.hora);
                        $('#output_castigo').val(r.nsancion);
                        $('#output_ffin').val(r.fecha_san);
                        $('#output_hfin').val(r.hora_san);
                        $('#btn_sancion').attr('disabled',true);
                        $('#btn_rdevolucion').hide();
                        $('#btn_aceptar_devolucion').show();}                    
                },'json');
            }
            function devolucion(data){
                $.post('<?php echo site_url('prestamo_reserva/devolucion'); ?>',{dato_devolucion: data, item_devolucion: $('#output_item').val()},function(r){
                    if(r){
                        $('#output_devolucion').val(r.devolucion);
                        $('#output_fentrega').val(r.fecha);
                        $('#output_hentrega').val(r.hora);
                        $('#btn_rdevolucion').hide();
                        $('#btn_aceptar_devolucion').show();}                    
                },'json');                
            }
            function fecha(){
                var fecha_actual = new Date();
                dia = fecha_actual.getDate();
                mes = fecha_actual.getMonth()+1;
                anio = fecha_actual.getFullYear();
                hora = fecha_actual.getHours();
                minuto = fecha_actual.getMinutes();
                segundo = fecha_actual.getSeconds();
                if (mes <= 9) {
                    mes = '0'+mes;
                }
                if (dia <= 9) {
                    dia = '0'+dia;
                }                
                if (minuto <= 9) {
                    minuto = '0'+minuto;
                }
                if (segundo <= 9) {
                    segundo = '0'+segundo;
                }
                $('#time').empty();				
                $('#time').append('<strong>'+hora+':'+minuto+'</strong> <em>'+anio+'-'+mes+'-'+dia+'</em>');
                setTimeout("fecha()",1000); //Genera ciclos
            }
            $().ready(function() {
                $('button').button()
                $('.seleccion').button({
                    icons:{
                        primary: "ui-icon-arrowreturnthick-1-e"
                    }
                })      
                $('.seleccion_another').button({
                    icons:{
                        primary: "ui-icon ui-icon-circle-triangle-e"
                    }
                })
                $('#output_item').click(function(){
                    $.post('<?php echo site_url('variado/ver_item'); ?>',{item: $('#output_item').val()},function(r){
                        if (r.isbn) {
                            jAlert('<b>ISBN: </b>'+r.isbn+'<br /><b>TITULO: </b>'+r.titulo+'<br /><b>AUTOR: </b>'+r.autor+'<br /><b>EDITORIAL: </b>'+r.editorial+'<br /><b>TEMATICA: </b>'+r.tematica+'<br /><b>FECHA: </b>'+r.fecha+'<br /><b>TERMINAL: </b>'+r.terminal+'<br />', 'INFORMACION BIBLIOGRÁFICA');
                        }
                    },'json');
                });
                $('#btn_aceptar_devolucion').click(function(){
                    document.location.href= '<?php echo site_url('prestamo_reserva/devolucion'); ?>';
                });
                $('#resultado').hide();
                fecha(); 
                $("#nino").click(function(){
                    jAlert('Promoción "Alan Turing" E.A.P. Ing. Informática<br /><br /><a href="http://about.me/ninosimeon">+ Info</a>','Nino Simeón');
                }); 
                $('.lista_devolucion').hide();
                $('#tbl_lista').show();
                $('#tbl_lista tr').mouseover(function(){
                    $(this).css('background-color','#FEEEBD');
                });
                $('#tbl_lista tr').mouseout(function(){
                    $(this).css('background-color','');
                });
                $('#btn_no').attr('disabled',false);
                $('#btn_castigo').attr('disabled',false);
                $('#btn_castigo').click(function(){
                    $('#btn_no').attr('disabled',true);
                    $('#btn_castigo').attr('disabled',true);
                    $('#btn_rdevolucion').attr('disabled',true);
                    $('#sancion').show();
                    $('#btn_sancion').attr('onclick','sancion('+$('#output_prestamo').val()+')');
                    $.post('<?php echo site_url('prestamo_reserva/devolucion'); ?>',{dato_sancion: $('#output_prestamo').val()},function(r){
                        $('#n_veces').val(r.nveces);
                    },'json');
                });
                $('#btn_no').click(function(){
                    $('#btn_no').attr('disabled',true);
                    $('#btn_castigo').attr('disabled',true);
                    $('#btn_rdevolucion').attr('disabled',false);
                });
            });
            function buscar(valor){
                jAlert('En progreso','DEVELOPING');
            }                         
            function carga_info(data){
                $.post('<?php echo site_url('prestamo_reserva/devolucion'); ?>',{click_busca: data},function(r){
                    $('#output_usuario').val(r.identificacion);
                    $('#output_item').val(r.isbn);
                },'json');
            }
        </script>
        <style>
            #info_usuario {
                float: left;
                width: 65%;
            }
            #time {
                float: right;
                width: 35%;
                font-size: 195%;
                font-weight: normal;
                line-height: 200%;
            }
            #datos_devolucion {
                float: left;
                width: 50%;
            }
            #cabecera_contenido {
                /*height: 11%;*/                
                padding: 3px;
                /*background-color: #6CF;*/
                border: 1px dashed #000;
            }
            #sancion {
                float: right;
                width: 50%;
            }
            #renovacion {
                clear: both;
            }
        </style>        
    </head>
    <body>    
        <div id="contenido" class="ui-widget">
            <div id="buscador" class="ui-widget-content ui-corner-all">Usuario: 
                <input type="text" name="usuarioBusca" id="usuarioBusca" />
                <button type="button" name="ir" id="ir" onclick="buscar($('#usuarioBusca').val())">Ir</button>
                <div id="resultado" style="color: #000">
                    <p><b>No encontrado! =(</b></p>
                </div>
            </div>
            <div id="titulo"><strong>LISTA DEVOLUCION</strong></div>
            <div id="cabezera"><img src="<?php echo base_url(); ?>public/img/bannerAdministrativo.png" width="800" height="67" alt="banner" /></div>
            <div id="menu" class="">
                <div>
                    <h4 class="ui-widget-header ui-corner-top">USUARIOS</h4>
                    <div class="ui-widget-content">                 
                        <?php echo anchor('usuarios/registrar', "<button class='seleccion'>Registrar</button>"); ?><br>
                        <?php echo anchor('usuarios/actualizar', "<button class='seleccion'>Actualizar</button>"); ?>
                    </div>
                    <h4 class="ui-widget-header">TRANSACCIONES</h4>
                    <div class="ui-widget-content">
                        <?php echo anchor('prestamo_reserva/solicitud', "<button class='seleccion'>Solicitud prestamo</button>"); ?><br>
                        <?php echo anchor('prestamo_reserva/prestamo', "<button class='seleccion'>Lista prestamo</button>"); ?><br>
                        <?php echo anchor('prestamo_reserva/devolucion', "<button class='seleccion'>Lista devolución</button>"); ?>
                    </div>
                    <h4 class="ui-widget-header">TRANSACCIONES</h4>
                    <div class="ui-widget-content ui-corner-bottom">
                        <?php echo anchor('material_auxiliar/sinconfirmar', "<button class='seleccion'>Sin confirmar</button>"); ?>                        
                    </div>              
                </div>
                <div id="otros_menu" class="" style="margin-top: 10px;">
                    <?php echo $menu; ?>                                 
                </div>
                <div id="terminal" class="ui-corner-all ui-widget-content">
                    TERMINAL:<br> 
                    <b><?php echo $sesion['nom_terminal']; ?></b>
                </div>
            </div>
            <footer id="pieDePagina" class="ui-state-default">
                <div style="float: left;">
                    Ciudad Universitaria - Av. Mercedes Indacochea N° 609<br />
                    Teléfono: 232-1338, Huacho - Perú
                </div>
                <div style="float: right">Desarrollado por: Nino D. Simeón Huaccho</div>                    
                <div style="clear: both;"></div>
            </footer>
            <div id="logeado" class="ui-widget-header">         
                <b><?php echo $sesion ['perfil_usuario']; ?>,</b> <?php echo $sesion ['apellidos_nombres']; ?> 
                <nav style="margin-right: 10px;float: right;">
                    <a href="<?php echo site_url('variado/panel'); ?>">Panel de usuario</a> | 
                    <a href="<?php echo site_url('variado/cerrar_sesion'); ?>">Cerrar Sesión</a>
                </nav>
            </div>
            <div id="terminal">TERMINAL:<br />
                <strong><?php echo $sesion['nom_terminal']; ?></strong></div>
            <div id="contenido_contenido">
                <div id="cabecera_contenido" class="ui-widget-content">
                    <div id="info_usuario">
                        <table width="100%" border="0" cellspacing="0" cellpadding="1">
                            <tr>
                                <td width="18%" scope="col"><strong>USUARIO:</strong></td>
                                <td width="82%" scope="col"><input name="output_usuario" type="text" id="output_usuario" style="width:95%" readonly="readonly"/></td>
                            </tr>
                            <tr>
                                <td><strong>ITEM:</strong></td>
                                <td><input name="output_item" type="text" id="output_item" style="width:70%" readonly="readonly"/></td>
                            </tr>
                        </table>
                    </div>
                    <div id="time"></div>
                    <div style="clear: both;">                      
                    </div>
                </div>
                <br />
                <div id="devolucion" style="" class="lista_devolucion">
                    <div id="datos_devolucion"><p><strong>DATOS DE DEVOLUCIÓN:</strong></p>
                        <table width="90%" border="0" align="center" cellpadding="2" cellspacing="0">
                            <tr>
                                <td width="50%" scope="col"><strong>N° DEVOLUCIÓN:</strong></td>
                                <td width="50%" scope="col"><input name="output_devolucion" type="text" id="output_devolucion"  size="5" readonly="readonly"/></td>
                            </tr>
                            <tr>
                                <td><strong>N° PRESTAMO:</strong></td>
                                <td><input name="output_prestamo" type="text" id="output_prestamo" size="5" readonly="readonly"/></td>
                            </tr>
                            <tr>
                                <td><strong>FECHA DE ENTREGA:</strong></td>
                                <td><input name="output_fentrega" type="text" id="output_fentrega"  size="15" readonly="readonly"/></td>
                            </tr>
                            <tr>
                                <td><strong>HORA DE ENTREGA:</strong></td>
                                <td><input name="output_hentrega" type="text" id="output_hentrega"  size="15" readonly="readonly"/></td>
                            </tr>
                            <tr>
                                <td><strong>SANCIÓN:</strong></td>
                                <td><input type="submit" name="btn_no" id="btn_no" value="No" />
                                    <input type="submit" name="btn_castigo" id="btn_castigo" value="Si" /></td>
                            </tr>
                            <tr>
                                <td colspan="2"><input type="button" name="btn_rdevolucion" id="btn_rdevolucion" value="&gt;REGISTRAR DEVOLUCIÓN&lt;" style="width:100%"/><input name="btn_aceptar_devolucion" type="button" value="ACEPTAR"  id="btn_aceptar_devolucion"/></td>
                            </tr>
                        </table>
                    </div>
                    <div id="sancion"><p><strong>SANCIÓN:</strong></p>
                        <table width="90%" border="0" align="center" cellpadding="2" cellspacing="0">
                            <tr>
                                <td width="32%" valign="top" scope="col"><strong>N° CASTIGO:</strong></td>
                                <td width="68%" valign="top" scope="col"><strong>
                                        <input name="output_castigo" type="text" id="output_castigo" size="4" readonly="readonly" /> 
                                        N° VECES: 
                                        <input name="n_veces" type="text" id="n_veces"  size="4" readonly="readonly" />
                                    </strong></td>
                            </tr>
                            <tr>
                                <td valign="top"><strong>TIPO:</strong></td>
                                <td><select name="input_tipo" id="input_tipo">
                                        <?php foreach ($sanciones->result() as $value) {
                                            ?><option value="<?php echo $value->codigo; ?>"><?php echo $value->descripcion; ?></option><?php } ?>
                                    </select></td>
                            </tr>
                            <tr>
                                <td valign="top"><strong>FECHA FIN:</strong></td>
                                <td><input name="output_ffin" type="text" id="output_ffin" size="15" readonly="readonly" /></td>
                            </tr>
                            <tr>
                                <td valign="top"><strong>HORA FIN:</strong></td>
                                <td><input name="output_hfin" type="text" id="output_hfin" size="15" readonly="readonly" /></td>
                            </tr>
                            <tr>
                                <td valign="top">&nbsp;</td>
                                <td><input type="button" name="btn_sancion" id="btn_sancion" value="Sanción" /></td>
                            </tr>
                        </table>
                    </div></div><div id="renovacion" class="lista_devolucion"><p><strong>INFORMACIÓN DE RENOVACIÓN DE PRESTAMO:</strong></p><table width="100%" border="0" cellspacing="0" cellpadding="3">
                        <tr>
                            <td width="18%" scope="col"><strong>N° PRESTAMO:</strong></td>
                            <td colspan="3" scope="col"><input name="output_rprestamo" type="text" id="output_rprestamo" style="width:10%" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <td><strong>USUARIO:</strong></td>
                            <td colspan="3"><input name="output_rusuario" type="text" id="output_rusuario" style="width:90%" readonly="readonly"/></td>
                        </tr>
                        <tr>
                            <td><strong>ITEM:</strong></td>
                            <td colspan="3"><input name="output_ritem" type="text" id="output_ritem" style="width:30%" readonly="readonly" /></td>
                        </tr>
                        <tr>
                            <td><strong>TIPO:</strong></td>
                            <td colspan="3"><input name="output_rtipo" type="text" id="output_rtipo" style="width:30%" readonly="readonly"/></td>
                        </tr>
                        <tr>
                            <td><strong>FECHA INICIO:</strong></td>
                            <td width="31%"><input name="output_rfechaini" type="text" id="output_rfechaini" style="width:80%" readonly="readonly"/></td>
                            <td width="16%"><strong>HORA INICIO:</strong></td>
                            <td width="35%"><input name="output_rhoraini" type="text" id="output_rhoraini" style="width:80%" readonly="readonly"/></td>
                        </tr>
                        <tr>
                            <td><strong>FECHA FIN:</strong></td>
                            <td><input name="output_rfechafin" type="text" id="output_rfechafin" style="width:80%" readonly="readonly"/></td>
                            <td><strong>FECHA FIN:</strong></td>
                            <td><input name="output_rhorafin" type="text" id="output_rhorafin" style="width:80%" readonly="readonly"/></td>
                        </tr>
                        <tr>
                            <td colspan="4"><input type="button" name="btn_rrenovacion" id="btn_rrenovacion" style="width:100%" value="&gt;REGISTRAR RENOVACIÓN&lt;" /><input name="btn_aceptar_renovacion" type="button" value="ACEPTAR" style="width:100%" onclick="cerrar()"/></td>
                        </tr>
                    </table>
                </div><table width="100%" border="0" cellspacing="0" cellpadding="2" id="tbl_lista" class="lista_devolucion">
                    <tr>
                        <th width="6%" bgcolor="#F0F0F0" scope="col">N°</th>
                        <th width="18%" bgcolor="#F0F0F0" scope="col">USUARIO</th>
                        <th width="18%" bgcolor="#F0F0F0" scope="col">ITEM</th>
                        <th width="9%" bgcolor="#F0F0F0" scope="col">TIPO</th>
                        <th width="12%" bgcolor="#F0F0F0" scope="col">FECHA D.</th>
                        <th width="10%" bgcolor="#F0F0F0" scope="col">HORA D.</th>
                        <th width="13%" bgcolor="#F0F0F0" scope="col">DEVOLUCIÓN</th>
                        <th width="14%" bgcolor="#F0F0F0" scope="col">RENOVACIÓN</th>
                    </tr>                    
                    <?php
                    if ($listado) {
                        foreach ($listado->result() as $value) {
                            ?>
                            <tr onclick="carga_info(<?php echo $value->nevento; ?>)">
                                <td align="center"><?php echo $value->nevento; ?></td>
                                <td align="center"><?php echo $value->usuario; ?></td>
                                <td align="center"><?php echo $value->idItem; ?></td>
                                <td align="center"><?php echo $value->modPrestamo; ?></td>
                                <td align="center"><?php echo $value->fechaFin; ?></td>
                                <td align="center"><?php echo $value->horaFin; ?></td>
                                <td align="center"><input type="button" name="btn_devolucion" id="btn_devolucion" value="✓" onclick="vista_devolucion(<?php echo $value->nevento; ?>)"/></td>
                                <td align="center"><input type="button" name="btn_renovacion" id="btn_renovacion" value="✓" onclick="vista_renovacion(<?php echo $value->nevento; ?>)"/></td>
                            </tr><?php }
            }
                    ?>
                </table>
            </div>
        </div>
    </body>
</html>